<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'dashboard', 'uses' => 'Controller@index']);
Route::post('/login', ['as' => 'user.auth', 'uses' => 'Controller@auth']);
Route::get('/login', ['as' => 'user.login', 'uses' => 'Controller@login']);

Route::get('share/{username?}', ['as' => 'user.share', 'uses' => 'Controller@share']);

Route::middleware(['auth'])->group(function () {
    Route::resources([
        'tag' => 'TagsController',
        'tag-category' => 'TagCategoriesController',
        'course' => 'CoursesController',
        'webinar' => 'WebinarsController',
        'audio' => 'AudioController',
        'material-category' => 'MaterialCategoriesController',
        'material' => 'MaterialsController',
        'notification' => 'NotificationsController',
        'device' => 'DevicesController',
        'program' => 'PartnerProgramsController',
        'user' => 'UsersController',
        'meeting' => 'MeetingsController',
        'qualification' => 'QualificationsController',
        'package' => 'PackageNotificationsController',
    ]);

    Route::get('/logout', ['as' => 'user.logout', 'uses' => 'Controller@logout']);

    Route::post('/tag-remove', ['as' => 'tag.remove', 'uses' => 'TagsController@remove']);

    Route::post('/tag-category-remove', ['as' => 'tag-category.remove', 'uses' => 'TagCategoriesController@remove']);

    Route::post('/course-remove', ['as' => 'course.remove', 'uses' => 'CoursesController@remove']);
    Route::post('/course-sequence', ['as' => 'course.sequence', 'uses' => 'CoursesController@sequence']);
    Route::resource('course.video', 'VideosController', ['except' => ['index']]);
    Route::get('course/{course_id}/video', ['as' => 'course.video.index', 'uses' => 'VideosController@list']);
    Route::post('/course-video-remove', ['as' => 'course.video.remove', 'uses' => 'VideosController@remove']);
    Route::post('/course-video-sequence', ['as' => 'course.video.sequence', 'uses' => 'VideosController@sequence']);
    Route::post('/course-video-details', ['as' => 'course.video.details', 'uses' => 'VideosController@details']);

    Route::post('/webinar-remove', ['as' => 'webinar.remove', 'uses' => 'WebinarsController@remove']);

    Route::post('/audio-remove', ['as' => 'audio.remove', 'uses' => 'AudioController@remove']);
    Route::post('/audio-sequence', ['as' => 'audio.sequence', 'uses' => 'AudioController@sequence']);
    Route::post('/audio-details', ['as' => 'audio.details', 'uses' => 'AudioController@details']);

    Route::post('/material-category-remove', ['as' => 'material-category.remove', 'uses' => 'MaterialCategoriesController@remove']);
    Route::post('/material-remove', ['as' => 'material.remove', 'uses' => 'MaterialsController@remove']);
    Route::post('/material-sequence', ['as' => 'material.sequence', 'uses' => 'MaterialsController@sequence']);

    Route::post('/notification-remove', ['as' => 'notification.remove', 'uses' => 'NotificationsController@remove']);

    Route::post('/device-remove', ['as' => 'device.remove', 'uses' => 'DevicesController@remove']);

    Route::post('/program-remove', ['as' => 'program.remove', 'uses' => 'PartnerProgramsController@remove']);
    Route::resource('program.link', 'LinksController', ['except' => ['index']]);
    Route::get('program/{program_id}/link', ['as' => 'program.link.index', 'uses' => 'LinksController@list']);
    Route::post('/program-link-remove', ['as' => 'program.link.remove', 'uses' => 'LinksController@remove']);

    Route::get('profile', ['as' => 'user.profile', 'uses' => 'UsersController@profile']);

    Route::post('/user-remove', ['as' => 'user.remove', 'uses' => 'UsersController@remove']);

    Route::get('/change-password', ['as' => 'user.password', 'uses' => 'UsersController@password']);

    Route::post('/user-change-password', ['as' => 'user.change.password', 'uses' => 'UsersController@changePassword']);

    Route::post('/meeting-remove', ['as' => 'meeting.remove', 'uses' => 'MeetingsController@remove']);
    Route::post('/qualification-remove', ['as' => 'qualification.remove', 'uses' => 'QualificationsController@remove']);
    Route::post('/package-remove', ['as' => 'package.remove', 'uses' => 'PackageNotificationsController@remove']);
});