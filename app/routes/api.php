<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'Controller@authApp');
    Route::post('access_token', 'Controller@authorize');
});


Route::group(['middleware' => ['auth:api', 'fw-block-blacklisted', 'fw-block-attacks']], function(){
    Route::resources([
        'course' => 'CoursesController',
        'audio' => 'AudioController',
        'material' => 'MaterialsController',
        'webinar' => 'WebinarsController',
        'notification' => 'NotificationsController',
        'contact' => 'ContactsController',
        'meeting' => 'MeetingsController',
        'qualification' => 'QualificationsController',
        'userVideo' => 'UserVideosController',
        'userAudio' => 'UserAudiosController',
        ]);
        
    Route::post('notification-status', ['as' => 'notification.status', 'uses' => 'NotificationsController@updateStatus']);
    Route::post('contact-register', ['as' => 'contact.register', 'uses' => 'ContactsController@register']);
    Route::post('contact-import-register', ['as' => 'contact.import.register', 'uses' => 'ContactsController@importRegister']);
    Route::get('contact-import-list', ['as' => 'contact.import.list', 'uses' => 'ContactsController@importList']);
    Route::post('contact-remove', ['as' => 'contact.remove', 'uses' => 'ContactsController@remove']);
    Route::post('contact-device', ['as' => 'contact.device', 'uses' => 'DevicesController@device']);
        
    Route::post('userVideo-register', ['as' => 'userVideo.register', 'uses' => 'UserVideosController@register']);
    Route::post('userAudio-register', ['as' => 'userAudio.register', 'uses' => 'UserAudiosController@register']);
    Route::post('package-update', ['as' => 'package.update', 'uses' => 'Controller@packageUpdate']);
});