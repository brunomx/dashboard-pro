<?php

namespace App\Presenters;

use App\Transformers\WebinarTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class WebinarPresenter.
 *
 * @package namespace App\Presenters;
 */
class WebinarPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new WebinarTransformer();
    }
}
