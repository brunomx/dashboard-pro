<?php

namespace App\Presenters;

use App\Transformers\PartnerProgramTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PartnerProgramPresenter.
 *
 * @package namespace App\Presenters;
 */
class PartnerProgramPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PartnerProgramTransformer();
    }
}
