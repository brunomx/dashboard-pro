<?php

namespace App\Presenters;

use App\Transformers\LinksTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LinksPresenter.
 *
 * @package namespace App\Presenters;
 */
class LinksPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LinksTransformer();
    }
}
