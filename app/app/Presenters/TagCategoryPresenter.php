<?php

namespace App\Presenters;

use App\Transformers\TagCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TagCategoryPresenter.
 *
 * @package namespace App\Presenters;
 */
class TagCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TagCategoryTransformer();
    }
}
