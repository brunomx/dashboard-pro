<?php

namespace App\Presenters;

use App\Transformers\DeviceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DevicePresenter.
 *
 * @package namespace App\Presenters;
 */
class DevicePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DeviceTransformer();
    }
}
