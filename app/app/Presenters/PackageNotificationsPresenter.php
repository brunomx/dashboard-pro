<?php

namespace App\Presenters;

use App\Transformers\PackageNotificationsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PackageNotificationsPresenter.
 *
 * @package namespace App\Presenters;
 */
class PackageNotificationsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PackageNotificationsTransformer();
    }
}
