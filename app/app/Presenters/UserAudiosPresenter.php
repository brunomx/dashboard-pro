<?php

namespace App\Presenters;

use App\Transformers\UserAudiosTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserAudiosPresenter.
 *
 * @package namespace App\Presenters;
 */
class UserAudiosPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserAudiosTransformer();
    }
}
