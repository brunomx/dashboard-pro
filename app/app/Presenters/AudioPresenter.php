<?php

namespace App\Presenters;

use App\Transformers\AudioTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AudioPresenter.
 *
 * @package namespace App\Presenters;
 */
class AudioPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AudioTransformer();
    }
}
