<?php

namespace App\Presenters;

use App\Transformers\UserNotificationsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserNotificationsPresenter.
 *
 * @package namespace App\Presenters;
 */
class UserNotificationsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserNotificationsTransformer();
    }
}
