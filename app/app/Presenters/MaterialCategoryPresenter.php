<?php

namespace App\Presenters;

use App\Transformers\MaterialCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MaterialCategoryPresenter.
 *
 * @package namespace App\Presenters;
 */
class MaterialCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MaterialCategoryTransformer();
    }
}
