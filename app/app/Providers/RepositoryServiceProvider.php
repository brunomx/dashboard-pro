<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(\App\Repositories\VideoRepository::class, \App\Repositories\VideoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CourseRepository::class, \App\Repositories\CourseRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TagRepository::class, \App\Repositories\TagRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TagCategoryRepository::class, \App\Repositories\TagCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CourseTagsRepository::class, \App\Repositories\CourseTagsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\VideoTagsRepository::class, \App\Repositories\VideoTagsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WebinarRepository::class, \App\Repositories\WebinarRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AudioRepository::class, \App\Repositories\AudioRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MaterialCategoryRepository::class, \App\Repositories\MaterialCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MaterialRepository::class, \App\Repositories\MaterialRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NotificationRepository::class, \App\Repositories\NotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DeviceRepository::class, \App\Repositories\DeviceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserNotificationsRepository::class, \App\Repositories\UserNotificationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PartnerProgramRepository::class, \App\Repositories\PartnerProgramRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LinksRepository::class, \App\Repositories\LinksRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ContactRepository::class, \App\Repositories\ContactRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MeetingRepository::class, \App\Repositories\MeetingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QualificationRepository::class, \App\Repositories\QualificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PackageRepository::class, \App\Repositories\PackageRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserVideosRepository::class, \App\Repositories\UserVideosRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PackageNotificationsRepository::class, \App\Repositories\PackageNotificationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserAudiosRepository::class, \App\Repositories\UserAudiosRepositoryEloquent::class);
        //:end-bindings:
    }
}
