<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CourseTags extends Model
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'course_id', 'tag_id'
    ];

    protected $hidden = [];
}
