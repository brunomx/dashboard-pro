<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Storage;

/**
 * Class Course.
 *
 * @package namespace App\Entities;
 */
class Course extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'image', 'sequence', 'free','package_id'
    ];

    protected $hidden = [
        'status', 'sequence', 'free', 'type', 'created_at', 'updated_at', 'deleted_at'
    ];
    
    public function getImageLinkAttribute(){
        return Storage::url($this->attributes['image']);
    }

    public function videos(){
        return $this->hasMany(Video::class)->orderBy('sequence');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class, 'course_tags')->whereNull('course_tags.deleted_at');
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }
}
