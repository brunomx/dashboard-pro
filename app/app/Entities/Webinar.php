<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Carbon\Carbon;


class Webinar extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'embed', 'url', 'start_date' , 'end_date',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function getFormattedStartDateAttribute(){
        $date  = new Carbon($this->attributes['start_date']);
        return $date->format('d/m/Y H:i');
    }
    public function getFormattedEndDateAttribute(){
        $date  = new Carbon($this->attributes['end_date']);
        return $date->format('d/m/Y H:i');
    }
}
