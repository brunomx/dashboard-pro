<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Storage;
use \Carbon\Carbon;

/**
 * Class Contact.
 *
 * @package namespace App\Entities;
 */
class Contact extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'id','first_name', 'last_name', 'phone', 'email', 'group', 'type', 'entrepreneur', 'credility', 'networking', 'empowerment', 'dreamer', 'register', 'observation', 'comments', 'invitation', 'plan', 'fup', 'closing', 'user_id', 'import_id', 'status'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function getStarsAttribute(){
        return $this->attributes['entrepreneur'] + $this->attributes['credility'] + $this->attributes['networking'] + $this->attributes['empowerment'] + $this->attributes['dreamer'] + $this->attributes['register'];
    }

    public function getFormattedInvitationAttribute(){
        if (is_null($this->attributes['invitation'])) {
            return $this->attributes['invitation'];
        } else {
            $date = new Carbon($this->attributes['invitation']);
            return $date->format('d/m/Y');
        }
    }

    public function getFormattedPlanAttribute(){
        if (is_null($this->attributes['plan'])) {
            return $this->attributes['plan'];
        } else {
            $date = new Carbon($this->attributes['plan']);
            return $date->format('d/m/Y');
        }
    }

    public function getFormattedFupAttribute(){
        if(is_null($this->attributes['fup'])){
            return $this->attributes['fup'];
        } else {
            $date  = new Carbon($this->attributes['fup']);
            return $date->format('d/m/Y');
        }
    }

    public function getFormattedClosingAttribute(){
        if(is_null($this->attributes['closing'])){
            return $this->attributes['closing'];
        } else {
            $date  = new Carbon($this->attributes['closing']);
            return $date->format('d/m/Y');
        }
    }

}
