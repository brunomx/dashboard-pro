<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PackageNotifications extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $fillable     = ['title', 'subtitle', 'message', 'before', 'days'];
    protected $hidden       = ['deleted_at', 'updated_at' , 'created_at'];

    public $daysRange = [0,1,2,3,4,5,6,7,8,9,10];
    public $tagList = ['[DATA]', '[DIA_SEMANA]', '[DIA_MES]', '[NOME]', '[PACK_NAME]', '[PACK_PRICE]'];

    public function getDiffAttribute(){
        if($this->attributes['before'] == '1'){
            return $this->attributes['days'];
        } else {
            return $this->attributes['days'] * -1;
        }
    }

    public function formattedMessage($user){
        if(strpos($this->attributes['message'], '[DATA]') !== false){
            $this->attributes['message'] = str_replace('[DATA]', $user->renewal_date, $this->attributes['message']);
        }

        if (strpos($this->attributes['message'], '[DIA_SEMANA]') !== false) {
            $this->attributes['message'] = str_replace('[DIA_SEMANA]', $user->renewal_day_week, $this->attributes['message']);
        }

        if (strpos($this->attributes['message'], '[DIA_MES]') !== false) {
            $this->attributes['message'] = str_replace('[DIA_MES]', $user->renewal_day, $this->attributes['message']);
        }

        if (strpos($this->attributes['message'], '[NOME]') !== false) {
            $this->attributes['message'] = str_replace('[NOME]', $user->first_name, $this->attributes['message']);
        }

        if (strpos($this->attributes['message'], '[PACK_NAME]') !== false) {
            $this->attributes['message'] = str_replace('[PACK_NAME]', $user->package->name, $this->attributes['message']);
        }

        if (strpos($this->attributes['message'], '[PACK_PRICE]') !== false) {
            $this->attributes['message'] = str_replace('[PACK_PRICE]', $user->package->price, $this->attributes['message']);
        }

        return $this->attributes['message'];
    }
}
