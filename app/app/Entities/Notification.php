<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Carbon\Carbon;

class Notification extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $table        = 'notifications';
    protected $fillable     = ['title', 'subtitle', 'message', 'link', 'system', 'schedule', 'publication', 'successful', 'failed', 'converted', 'remaining', 'integration_id', 'package_id', 'type'];
    protected $hidden       = ['deleted_at', 'updated_at' , 'created_at', 'users'];

    public $systemName = ['Todos', 'Android', 'IOS'];

    public function getSystemNameAttribute(){
        return $this->systemName[$this->attributes['system']];
    }

    public function getFormattedPublicationAttribute(){
        $date  = new Carbon($this->attributes['publication']);
        return $date->format('d/m/Y H:i');
    }
    
    public function users(){
        return $this->belongsToMany(User::class, 'user_notifications');
    }
}
