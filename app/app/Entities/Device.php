<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Device extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $fillable     = ['manufacturer', 'model', 'platform', 'serial', 'udid', 'version', 'player_ids', 'user_id', 'token'];
    protected $hidden       = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

}
