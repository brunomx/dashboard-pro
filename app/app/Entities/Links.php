<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Links extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'code', 'link', 'image', 'partner_program_id'
    ];

    protected $hidden = [];

    public function program(){
        return $this->belongsTo(PartnerProgram::class);
    }
}
