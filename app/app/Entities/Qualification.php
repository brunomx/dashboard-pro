<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Qualification extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $fillable     = ['name', 'city', 'state', 'level', 'month', 'year', 'image'];
    protected $hidden       = ['deleted_at', 'updated_at' , 'created_at'];
    public $options         = ['SILVER', 'GOLD', 'PLATINUM', 'RUBY', 'SAPHIRE', 'TITANIUM', 'DIAMOND', 'ADVISOR', 'BLACK ADVISOR', 'BROKER'];
    public $months          = ['Selecione', 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

    public function getImageLinkAttribute(){
        return Storage::url($this->attributes['image']);
    }

    public function getLevelNameAttribute(){
        return $this->options[$this->attributes['level']];
    }
}
