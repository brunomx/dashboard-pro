<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserVideos extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $fillable     = ['user_id', 'video_id', 'watch_time', 'completed'];
    protected $hidden       = ['deleted_at', 'updated_at' , 'created_at'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function video(){
        return $this->belongsTo(Video::class, 'video_id');
    }
}
