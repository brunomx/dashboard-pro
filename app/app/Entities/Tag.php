<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


class Tag extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $table        = 'tags';
    protected $fillable     = [
        'name', 'description'
    ];
    protected $hidden = [
        'category_id', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function category(){
        return $this->belongsTo(TagCategory::class);
    }
}
