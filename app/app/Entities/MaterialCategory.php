<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MaterialCategory extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $table        = 'material_categories';
    protected $fillable     = ['name'];
    protected $hidden       = [];

    public function materials(){
        return $this->hasMany(Material::class, 'category_id');
    }
}
