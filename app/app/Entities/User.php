<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Carbon\Carbon;

class User extends Authenticatable implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;
    use HasApiTokens;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'cpf', 'rg', 'birth', 'gender', 'username', 'access_token', 'refresh_token', 'password', 'permission', 'package_id', 'package_renewal'
    ];

    protected $hidden = [
        'email_verified_at', 'password', 'remember_token', 'notes', 'legacy_id', 'status', 'created_at', 'updated_at', 'deleted_at', 'tokens' 
    ];

    private $days = ['Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Saturday'];

    public function getNameAttribute(){
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    public function getDiffRenewalAttribute(){
        $date = new Carbon($this->attributes['package_renewal']);
        $now = Carbon::now();

        return $now->diffInDays($date, false);
    }

    public function getRenewalDateAttribute(){
        $date  = new Carbon($this->attributes['package_renewal']);
        return $date->format('d/m/Y');
    }

    public function getRenewalDayAttribute(){
        $date  = new Carbon($this->attributes['package_renewal']);
        return $date->format('d');
    }

    public function getRenewalDayWeekAttribute(){
        $date  = new Carbon($this->attributes['package_renewal']);
        return $this->days[$date->format('w')];
    }

    public function device(){
        return $this->hasOne(Device::class);
    }

    public function package(){
        return $this->hasOne(Package::class, 'id');
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }
}
