<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserNotifications extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $table        = 'user_notifications';
    protected $fillable     = ['id', 'user_id', 'notification_id', 'status'];
    protected $hidden       = [];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function notification(){
        return $this->belongsTo(Notification::class, 'notification_id')->whereNull('notifications.deleted_at');;
    }
}
