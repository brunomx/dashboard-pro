<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Storage;

class Audio extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'url', 'type', 'path' , 'image', 'sequence', 'package_id', 'size', 'duration'
    ];

    protected $hidden = [
        'sequence', 'created_at', 'updated_at', 'deleted_at'
    ];

    private $typeName = ['Motivacional', 'Produto'];

    public function getImageLinkAttribute(){
        return Storage::url($this->attributes['image']);
    }

    public function getPathLinkAttribute(){
        return Storage::url($this->attributes['path']);
    }

    public function getTypeNameAttribute(){
        return $this->typeName[$this->attributes['type']];
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function users(){
        return $this->hasMany(UserAudios::class, 'audio_id');
    }
}