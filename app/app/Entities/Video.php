<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Video extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    protected $fillable = [
        'name', 'embed', 'thumbnail', 'url', 'audio', 'publication', 'before_id', 'course_id', 'package_id', 'duration', 'sequence'
    ];

    protected $hidden = [
        'free', 'share', 'size', 'publication', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function tags(){
        return $this->belongsToMany(Tag::class, 'video_tags')->whereNull('video_tags.deleted_at');
    }

    public function permissions(){
        return $this->belongsToMany(Tag::class, 'video_tags')->where('video_tags.type' , 0)->whereNull('video_tags.deleted_at');
    }

    public function timeTags(){
        return $this->hasMany(VideoTags::class, 'video_id')->where('video_tags.type' , 1)->whereNull('video_tags.deleted_at');
    }

    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function before(){
        return $this->belongsTo(Video::class, 'before_id');
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function users(){
        return $this->hasMany(UserVideos::class, 'video_id');
    }
}
