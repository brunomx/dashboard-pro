<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Meeting extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $fillable     = ['name', 'date', 'description', 'package_id', 'image', 'address_address', 'address_latitude', 'address_longitude'];
    protected $hidden       = ['deleted_at', 'updated_at' , 'created_at'];

    public function getFormattedDateAttribute(){
        $date  = new Carbon($this->attributes['date']);
        return $date->format('d/m H:i');
    }
   
    public function getImageLinkAttribute(){
        return Storage::url($this->attributes['image']);
    }
    
    public function getDiffDaysAttribute(){
        $date = new Carbon($this->attributes['date']);
        $now = Carbon::now();

        $diff = $now->diffInHours($date, false);
        return $diff;
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }
}
