<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Support\Facades\Storage;

class Material extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;
    use Notifiable;

    public    $timestamps   = true;
    protected $table        = 'materials';
    protected $fillable     = ['name', 'path', 'embed', 'url', 'size', 'sequence', 'publication', 'type', 'category_id', 'package_id'];
    protected $hidden       = ['deleted_at', 'created_at', 'updated_at'];

    public $typeName = ['PDF', 'Áudio', 'Vídeo', 'Slide'];

    public function getTypeNameAttribute(){
        return $this->typeName[$this->attributes['type']];
    }

    public function category(){
        return $this->belongsTo(MaterialCategory::class);
    }

    public function getPathLinkAttribute(){
        return Storage::url($this->attributes['path']);
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }
}
