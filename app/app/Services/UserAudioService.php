<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\UserAudiosRepository;
use App\Repositories\AudioRepository;
use App\Validators\UserAudiosValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class UserAudioService
{
	private $repository;
	private $repositoryAudio;
	private $validator;
	
	public function __construct(UserAudiosRepository $repository, UserAudiosValidator $validator, AudioRepository $repositoryAudio)
	{
		$this->repository = $repository;
		$this->repositoryAudio = $repositoryAudio;
		$this->validator  = $validator;
	}

	public function store(array $data, $user)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if($data['duration'] > 0){
				$teste = $this->repositoryAudio->update(array('duration' => $data['duration']), $data['audio_id']);
			}

			$data['user_id'] = $user->id;
			
			$result = $this->repository->findByField(['user_id' => $user->id, 'audio_id' => $data['audio_id']]);

			if ($result->count() > 0) {
				if($result->first()->completed == '0'){
					if($data['watch_time'] >= $result->first()->watch_time){
						$this->repository->update($data, $result->first()->id);
					}
				}
			} else {
				$this->repository->create($data);
			}
			
			return [
				'success' 	=> true,
				'messages' 	=> "Dados cadastrados",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			var_dump($e->getMessage());die();
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
}