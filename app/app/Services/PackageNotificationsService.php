<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\PackageNotificationsRepository;
use App\Validators\PackageNotificationsValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Entities\PackageNotifications;

class PackageNotificationsService
{
    private $repository;
	private $validator;

	public function __construct(PackageNotificationsRepository $repository, PackageNotificationsValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	
	public function store(array $data)
	{
		try {			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$data['message'] = trim($data['message']);
			$package = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Notificação de pacote cadastrada",
				'data' 	  	=> $package,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
    
    public function update(array $data, $id)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$package = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Notificação de pacote atualizada",
				'data' 	  	=> $package,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Notificação de pacote deletada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}
}