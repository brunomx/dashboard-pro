<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\MaterialRepository;
use App\Validators\MaterialValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;
use App\Entities\Material;

class MaterialService
{
    private $repository;
	private $validator;

	public function __construct(MaterialRepository $repository, MaterialValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	public function store($request)
	{
		try {
			$data = $request->all();

			if ($request->has('pdf')) {
				$data['path'] = $request->file('pdf')->store('public');
				$data['size'] = $request->file('pdf')->getSize();
			}

			if ($request->has('audio')) {
				$data['path'] = $request->file('audio')->store('public');
				$data['size'] = $request->file('audio')->getSize();
			}

			if ($request->has('slide')) {
				$data['path'] = $request->file('slide')->store('public');
				$data['size'] = $request->file('slide')->getSize();
			}

			if (!is_null($data['embed'])) {
				$data['path'] = $this->getThumbnail($data['embed']);
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
				if (!is_null($data['url'])) {
					$data['size'] = $this->getSizeFromUrl($data['url']);
				}
			}
			
			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$material = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Material cadastrado",
				'data' 	  	=> $material,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update($request, int $id)
	{
		try {
			$data = $request->all();
			$material = $this->repository->find($id);
			if ($data['remove-pdf'] == '1') {
				if (Storage::disk('s3')->exists($material->path)) {
					Storage::delete($material->path);
					$data['path'] = null;
				}
			}

			if ($request->has('pdf')) {
				$data['path'] = $request->file('pdf')->store('public');
				$data['size'] = $request->file('pdf')->getSize();
			}

			if ($data['remove-audio'] == '1') {
				if (Storage::disk('s3')->exists($material->path)) {
					Storage::delete($material->path);
					$data['path'] = null;
				}
			}

			if ($request->has('audio')) {
				$data['path'] = $request->file('audio')->store('public');
				$data['size'] = $request->file('audio')->getSize();
			}

			if ($data['remove-slide'] == '1') {
				if (Storage::disk('s3')->exists($material->path)) {
					Storage::delete($material->path);
					$data['path'] = null;
				}
			}

			if ($request->has('slide')) {
				$data['path'] = $request->file('slide')->store('public');
				$data['size'] = $request->file('slide')->getSize();
			}

			if (!is_null($data['embed'])) {
				$data['path'] = $this->getThumbnail($data['embed']);
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
				if (!is_null($data['url'])) {
					$data['size'] = $this->getSizeFromUrl($data['url']);
				}
			}

			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$material = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Material atualizado",
				'data' 	  	=> $material,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Material deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function sequence($data)
	{
		try {
			$itens = json_decode($data);
			foreach($itens as $key => $item){
				$this->repository->update(array('sequence' => $key), $item);
			}
			return [
				'success' 	=> true,
				'messages' 	=> "Sequencia atualizada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
			
		}
	}

	public function getFromUser($user){
		if (is_null($user->package_id)) {
			$materiais = Material::orderBy('sequence')->where('package_id', null)->get();
		} else {
			$materiais = Material::orderBy('sequence')->where('package_id', null)->orWhere('package_id', '<=', $user->package_id)->get();
		}

		return $materiais;
	}

	private function getThumbnail($embed){
		preg_match('/src="([^"]+)"/', $embed, $match);
		$url = $match[1];

		$url = explode('/', $url);

		$imgid = $url[4];

		$retorno = json_decode(file_get_contents("https://vimeo.com/api/oembed.json?url=https://vimeo.com/$imgid"));

		return $retorno->thumbnail_url;
	}

	private function getSizeFromUrl($url){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		curl_exec($ch);
		$filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		curl_close($ch);

		return $filesize;
	}
}