<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\ContactRepository;
use App\Validators\ContactValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use function preg_replace;
use function explode;
use function implode;
use function array_reverse;

class ContactService
{
    private $repository;
	private $validator;

	public function __construct(ContactRepository $repository, ContactValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}

	public function store(array $data, $user)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			$data_contact = array(
							'first_name' => $data['name'], 
							'last_name' => $data['nickname'], 
							'phone' => $this->formattedPhone($data['phone']), 
							'email' => $data['email'], 
							'group' => $data['group'], 
							'type' => $data['types'], 
							'entrepreneur' => $data['entrepreneur'], 
							'credility' => $data['credility'], 
							'networking' => $data['networking'], 
							'empowerment' => $data['empowerment'], 
							'dreamer' => $data['dreamer'], 
							'register' => $data['register'], 
							'observation' => $data['observations'], 
							'comments' => $data['comments'], 
							'invitation' => $this->formattedData($data['invitation']), 
							'plan' => $this->formattedData($data['plan']), 
							'fup' => $this->formattedData($data['fup']), 
							'closing' => $this->formattedData($data['closing']), 
							'import_id' => $data['import_id'],
							'status' => '0',
							'user_id' => $user->id
							);

			if(is_null($data['id'])){
				$contact = $this->repository->create($data_contact);
			} else {
				$contact = $this->repository->update($data_contact, $data['id']);
			}

			return [
				'success' 	=> true,
				'messages' 	=> "Contato cadastrado",
				'data' 	  	=> $contact,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}
	
	public function storeImport(array $data, $user)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$create = 0;
			$update = 0;
			
			foreach ($data['contacts'] as $item) {
				$data_contact = array(
								'first_name' => $item['name'],
								'last_name' => $item['nickname'],
								'phone' => $this->formattedPhone($item['phone']),
								'email' => $item['email'],
								'status' => '1',
								'import_id' => $item['import_id'],
								'user_id' => $user->id,
								);
				
				$result = $this->repository->findByField('import_id',  $item['import_id']);
				if($result->count() > 0){
					$contact = $this->repository->update($data_contact, $result->first()->id);
					$update += 1;
				} else {
					$contact = $this->repository->create($data_contact);
					$create += 1;
				}
			}

			return [
				'success' 	=> true,
				'messages' 	=> "Contatos Importados",
				'data' 	  	=> array('create' => $create, 'update' => $update),
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update(array $data, int $id)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$contact = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Contato atualizado",
				'data' 	  	=> $contact,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Contato deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	private function formattedPhone($phone){
		if(empty($phone)){
			return $phone;
		} else {
			$phone = preg_replace('/[^0-9]/', '', $phone);
			return $phone;
		}
	}

	private function formattedData($data){
		if(is_null($data)){
			return null;
		} else {
			return implode('/', array_reverse(explode('/', $data)));
		}
	}
}