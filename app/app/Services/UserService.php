<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\PackageRepository;
use App\Validators\UserValidator;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Entities\User;
use App\Entities\PackageNotifications;
use \Carbon\Carbon;
use App\Services\NotificationService;

class UserService
{
    private $repository;
    private $validator;
    private $packageRepository;
    private $notificationService;

    public function __construct(UserRepository $repository, UserValidator $validator, PackageRepository $packageRepository, NotificationService $notificationService)
    {
        $this->repository = $repository;
        $this->packageRepository = $packageRepository;
        $this->validator = $validator;
        $this->notificationService = $notificationService;
    }

    public function store($request)
    {
        try {
            $data = $request->all();
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $data['password'] = Hash::make($data['email']);
            $data['permission'] = 'app.admin';

            $this->repository->create($data);

            return [
                'success' => true,
                'messages' => "Usuário cadastrado",
                'data' => null,
            ];
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function update($request, int $id)
    {
        try {
            $data = $request->all();

            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->repository->update($data, $id);

            return [
                'success' => true,
                'messages' => "Dados atualizados",
                'data' => null,
            ];
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function search(array $data)
    {
        try {
            $devices = Device::where('description', 'LIKE', '%' . $data['description'] . '%')->get();

            return [
                'success' => true,
                'messages' => "Itens found",
                'data' => $devices,
            ];
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

        }
    }

    public function delete(int $id)
    {
        try {
            $this->repository->delete($id);

            return [
                'success' => true,
                'messages' => "Deleted user",
                'data' => null,
            ];
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

        }
	}
	
	public function loginXpert(array $data)
	{
		try {
            $xpert = $this->getXperData($data);
			if(property_exists($xpert, 'access_token')){
                $data_user = array(
                    'first_name' => $xpert->member->first_name,
                    'last_name' => $xpert->member->last_name,
                    'email' => $xpert->member->email,
                    'username' => $xpert->member->username,
                    'pack' => $xpert->member->username,
                    'password' => Hash::make($data['password']),
                    'access_token' => $xpert->access_token,
                    'refresh_token' => $xpert->refresh_token,
                );
                
                if(property_exists($xpert->member, 'current_package')){
                    if(!empty($xpert->member->current_package)){
                        $package = $this->packageRepository->findWhere(['price' => $xpert->member->current_package->price]);
                        if($package->count() > 0){
                        $data_user['package_id'] = $package->first()->id;
                        }
                    }
                }
                            
                $result = $this->repository->findWhere(['username' => $xpert->member->username, 'email' => $xpert->member->email]);
                
                if($result->count() > 0){
                    $user = $this->repository->update($data_user, $result->first()->id);
                } else {
                    $user = $this->repository->create($data_user);
                }

				return [
					'success' => true,
					'messages' => "Usuario criado",
					'data' => $user,
				];
			} else {
				return [
					'success' => false,
					'messages' => "Usuario não encontrado",
					'data' => null,
				];

			}
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
    }
    
    public function checkPackage(array $data, $user){
        if(is_null($user->package_id)){
            $xpert = $this->getXperData($data);
            if(!is_null($xpert)){
                if(property_exists($xpert, 'access_token')){
                    if (property_exists($xpert->member, 'current_package')) {
                        if(!empty($xpert->member->current_package)){
                            $package = $this->packageRepository->findWhere(['price' => $xpert->member->current_package->price]);
                            if ($package->count() > 0) {
                                $this->repository->update(array('package_id' => $package->first()->id), $user->id);
                            }
                        }
                    }
                }
            }
        }
    }

	private function getXperData($data){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://myxpert.me/api/auth/login",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{"username": "'.$data['username'].'","password":  "'.$data['password'].'"}',
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"cache-control: no-cache",
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		return json_decode($response);
    }
    
    public function packageUpdate(array $data){
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $result = $this->repository->findWhere(['username' => $data['username'], 'email' => $data['email']]);

            if($result->count() > 0){
                $user = $result->first();
                $package = $this->packageRepository->findWhere(['price' => $data['package_price']]);
                if ($package->count() > 0) {
                    $this->repository->update(array('package_id' => $package->first()->id, 'package_renewal' => $data['package_date']), $user->id);
                    return ['success' => true, 'messages' => "User data updated"];
                } else {
                    return ['success' => false, 'messages' => 'Package not found'];
                }
            } else {
                return ['success' => false, 'messages' => 'User not found'];
            }
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function password($password, int $id)
    {
        try {
            $this->repository->update(array(
                'password' => Hash::make($password),
            ), $id);

            return [
                'success' => true,
                'messages' => "Senha atualizada",
                'data' => null,
            ];
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function getUsersPackExpired(){
        try {
            $users = User::where('package_renewal', '<>', null)->whereBetween('package_renewal', [Carbon::now()->subDays(11), Carbon::now()->addDays(11)])->get();

            $packages = PackageNotifications::all();
            $count = 0;
            foreach ($users as $user) {
                foreach ($packages as $package) {
                    if($user->diff_renewal == $package->diff){
                        $this->notificationService->generateMessage($user, $package);
                        $count++;
                    }
                }
            }
            return $count;
        } catch (Exception $e) {
            dd($e);
        }
    }
}
