<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\CourseRepository;
use App\Validators\CourseValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;
use App\Entities\CourseTags;
use App\Entities\Video;
use App\Entities\Course;
use App\Entities\UserVideos;
use App\Repositories\CourseTagsRepository;
use App\Services\CourseTagsService;

class CourseService
{
	private $repository;
	private $repositoryCourseTags;
	private $validator;
	private $courseTagsService;
	
	public function __construct(CourseRepository $repository, CourseValidator $validator, CourseTagsService $courseTagsService)
	{
		$this->repository 			= $repository;
		$this->validator 			= $validator;
		$this->courseTagsService 	= $courseTagsService;
	}

	public function store($request)
	{
		try {
			$data = $request->all();
			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
			
			$course = $this->repository->create($data);
			
			if ($request->has('tags')) {
				$this->courseTagsService->store($data['tags'], $course->id);
			}
			return [
				'success' 	=> true,
				'messages' 	=> "Curso cadastrado",
				'data' 	  	=> $course,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
	
	public function update($request, int $id)
	{
		try {
			$data = $request->all();
			$course = $this->repository->find($id);
			
			if($data['remove-image'] == '1'){
				if (Storage::disk('local')->exists($course->image)) {
					Storage::delete($course->image);
					$data['image'] = null;
				}
			}
			
			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}

			if($course->tags->count() > 0){
				$this->courseTagsService->update($request, $course);
			} else {
				if ($request->has('tags')) {
					$this->courseTagsService->store($data['tags'], $id);
				}
			}
			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$course = $this->repository->update($data, $id);
			
			return [
				'success' 	=> true,
				'messages' 	=> "Curso atualizado",
				'data' 	  	=> $course,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
	
	public function delete(int $id)
	{
		try {
			$course = $this->repository->find($id);
			if (Storage::disk('local')->exists($course->image)) {
				Storage::delete($course->image);
			}
			
			$tag = $this->repository->delete($id);
			
			return [
				'success' 	=> true,
				'messages' 	=> "Curso deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
	
	public function sequence($data)
	{
		try {
			$itens = json_decode($data);
			foreach($itens as $key => $item){
				$this->repository->update(array('sequence' => $key), $item);
			}
			return [
				'success' 	=> true,
				'messages' 	=> "Sequencia atualizada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}

	public function getVideosFromUser($user, $course){
		if (is_null($user->package_id)) {
			$videos = Video::orderBy('sequence')->where(['package_id' => null, 'course_id' => $course->id])->get();
		} else {
			$this->package_id = $user->package_id;
			$videos = Video::orderBy('sequence')->where('course_id', $course->id)->where(function ($query) {$query->where('package_id', null)->orWhere('package_id', '<=', $this->package_id);})->get();
		}

		foreach ($videos as $video) {
			$result = UserVideos::where(['user_id' => $user->id, 'video_id' => $video->id])->get();
			if ($result->count() > 0) {
				$video->watch_time = $result->first()->watch_time;
				if($result->first()->completed == '1'){
					$video->progress = 100;
				} else {
					if($video->watch_time == 0 || $video->duration == null){
						$video->progress = 0;
					} else {
						$video->progress = ($video->watch_time / $video->duration) * 100;
					}
				}
			} else {
				$video->progress = 0;
				$video->watch_time = 0;
			}

			$video->watch = true;

			if(!is_null($video->before)){
				$video->watch = false;
				$before = UserVideos::where(['user_id' => $user->id, 'video_id' => $video->before->id])->get();
				if ($before->count() > 0) {
					if($before->first()->completed == '1'){
						$video->watch = true;
					}
				}
			}
		}
		return $videos;
	}

	public function getFromUser($user){
		if (is_null($user->package_id)) {
			$audios = Course::orderBy('sequence')->where('type', 0)->where('package_id', null)->get();
		} else {
			$audios = Course::orderBy('sequence')->where('type', 0)->where('package_id', null)->orWhere('package_id', '<=', $user->package_id)->get();
		}

		return $audios;
	}
}