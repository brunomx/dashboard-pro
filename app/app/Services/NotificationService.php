<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\NotificationRepository;
use App\Repositories\UserNotificationsRepository;
use App\Validators\NotificationValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Entities\Notification;
use App\Entities\UserNotifications;
use App\Entities\User;
use \Carbon\Carbon;
use \DateTime;

class NotificationService
{
    private $repository;
    private $userNotificationsRepository;
	private $validator;
	private $app_id;
	private $restapp_id;
	public $user_id;

	public function __construct(NotificationRepository $repository, NotificationValidator $validator, UserNotificationsRepository $userNotificationsRepository)
	{
		$this->repository 					= $repository;
		$this->userNotificationsRepository 	= $userNotificationsRepository;
		$this->validator 					= $validator;

		$this->app_id = env('API_NOTIFICATION');
		$this->restapp_id = env('REST_API_NOTIFICATION');

		// $this->app_id = '38b24267-28b6-4ba8-b4e8-daac8bd07e92';
		// $this->restapp_id = 'ZjdlN2VhNWYtMzhmNC00OGU5LTg5MjYtYTM4ZmFjNzdmMThm';
	}

	public function store($request)
	{
		try {
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$notification = $this->repository->create($data);

			if ($request->has('users')) {
				foreach ($data['users'] as $key => $user) {
					$data_user_notifications = array('user_id' => $user, 'notification_id' => $notification->id);
					$this->userNotificationsRepository->create($data_user_notifications);
				}
			}

			if(!is_null($data['package_id'])){
				$users = User::where('package_id' , '<=', $data['package_id'])->get();
				foreach ($users as $key => $user) {
					$data_user_notifications = array('user_id' => $user->id, 'notification_id' => $notification->id);
					$this->userNotificationsRepository->create($data_user_notifications);
				}
			}

			$this->sendNotifications($notification);
			
			return [
				'success' 	=> true,
				'messages' 	=> "Notificação cadastrada",
				'data' 	  	=> $notification,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update(array $data, int $id)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$notification = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Notificação atualizada",
				'data' 	  	=> $notification,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function getFromUser(int $user_id){
		try {
			$this->user_id = $user_id;
			$notifications_user = $this->userNotificationsRepository->findWhere(['user_id' => $user_id])->transform(function ($item) {
									$item->notification->status =  $item->status;
									return $item->notification;
							});
			$notifications = $this->repository->all()->filter(function ($item, $key) {
    							return array_search( $this->user_id, $item->users->pluck('id')->toArray()) === false;
							});

			foreach ($notifications as $item) {
				$item->status = "0";
				$notifications_user->push($item);
			}

			return $notifications_user->sortByDesc('publication')->values();

		} catch (Exception $e) {
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Notificação deletada",
				'data' 	  	=> NULL,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function getDetails(int $id){
		$notification = Notification::find($id);
		
		if(!is_null($notification->integration_id)){
			$delivery = $this->getDeliveryData($notification->integration_id);

			if(property_exists($delivery, 'successful')){
				$notification->successful = $delivery->successful;
				$notification->failed = $delivery->failed;
				$notification->converted = $delivery->converted;
				$notification->remaining = $delivery->remaining;
				$notification->total = $delivery->successful + $delivery->failed + $delivery->remaining;
			}
		}

		return $notification;
	}

	public function updateStatus($data, $user){
		try {
			$notification = $this->userNotificationsRepository->findWhere(['user_id' => $user->id, 'notification_id' => $data['notification_id']])->first();

			if(is_null($notification)){
				$data['user_id'] = $user->id;
				$notification = $this->userNotificationsRepository->create($data);
			} else {
				$notification = $this->userNotificationsRepository->update($data, $notification->id);
			}

			return [
				'success' => true,
				'messages' => "Notificação atualizada",
				'data' => $notification,
			];
		} catch (Exception $e) {
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}

	public function generateMessage($user, $package)
	{
		try {
			$package->message = $package->formattedMessage($user);

			$data = Carbon::now()->format('Y-m-d') . ' 10:00:00';

			$notification = $this->repository->create(array('title' => $package->title,'subtitle' => $package->subtitle, 'message' => $package->message, 'system' => 0, 'publication' => $data, 'type' => 1));

			$this->userNotificationsRepository->create(array('user_id' => $user->id, 'notification_id' => $notification->id));

			$this->sendNotifications($notification);
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
    }

	private function sendNotifications($notification){
		try {
			if($notification->users->count() > 0){
				$player_ids = array();
				foreach ($notification->users as $user) {
					if(!is_null($user->device)){
						array_push($player_ids, $user->device->player_ids);
					}
				}
			} else {
				$player_ids = null;
			}

			$date = new Carbon($notification->publication);

			$fields = array(
				'app_id' => $this->app_id,
				'contents' => array("en" => $notification->message == null ? '' : $notification->message),
				'headings' => array("en" => $notification->title == null ? '' : $notification->title),
				'subtitle' =>  array("en" => $notification->subtitle == null ? '' : $notification->subtitle),
				'send_after' => $date->format('D M j Y G:i:s \U\T\C\-0300'),
				'delayed_option' => 'timezone'
			);

			if (!is_null($player_ids)) {
				$fields['include_player_ids'] = $player_ids;
			} else {
				$fields['included_segments'] = array('All');
				if ($notification->system == '1') {
					$fields['isAndroid'] = true;
				} else if ($notification->system == '2') {
					$fields['isIos'] = true;
				}
			}

			if(!is_null($notification->link)){
				$fields['app_url'] = $notification->link;
			}

			$fields['data'] = array('id' => $notification->id);

			$fields = json_encode($fields);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
				'Authorization: Basic ' . $this->restapp_id));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$response = curl_exec($ch);

			curl_close($ch);

			$data = json_decode($response, true);

			$this->repository->update(array('integration_id' => $data['id']), $notification->id);
		} catch (Exception $e) {
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}

	private function getDeliveryData($id){
		$appid = env('API_NOTIFICATION');
		$restappid = env('REST_API_NOTIFICATION');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications/$id?app_id=$this->app_id");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
			"Authorization: Basic $this->restapp_id"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$response = curl_exec($ch);
		curl_close($ch);

		return json_decode($response);
	}
}