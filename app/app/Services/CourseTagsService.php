<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use App\Repositories\CourseTagsRepository;
use Illuminate\Http\Request;
use App\Entities\Course;

class CourseTagsService
{
    private $repository;
    
    public function __construct(CourseTagsRepository $repository)
    {
        $this->repository = $repository;
    }
    public function store(array $tags, int $course_id)
    {
        try {
            if(!empty($tags)){
                foreach ($tags as $tag) {
                    $data_tags = array(
                        'course_id' => $course_id,
                        'tag_id' => $tag,
                    );
                    $this->repository->create($data_tags);
                }
            }
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }
    
    public function update(Request $request, Course $course)
    {
        try {
            $old_tags = $course->tags->pluck('id')->toArray();
            foreach ($old_tags as $item) {
                if ($request->has('tags')) {
                    if (in_array($item, $request->get('tags')) === false) {
                        $this->delete($item);
                    }
                } else {
                    $this->delete($item);
                }
            }
            
            if ($request->has('tags')) {
                foreach ($request->get('tags') as $tag) {
                    if (in_array($tag, $old_tags) === false) {
                        $data_tags = array(
                            'course_id' => $course->id,
                            'tag_id' => $tag,
                        );
                        $this->repository->create($data_tags);
                    }
                }
            }
            
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
            
        }
    }
    
    public function delete(int $tag_id)
    {
        try {
            $this->repository->deleteWhere(['tag_id' => $tag_id]);
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
            
        }
    }
}
