<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\QualificationRepository;
use App\Validators\QualificationValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;

class QualificationService
{
    private $repository;
	private $validator;

	public function __construct(QualificationRepository $repository, QualificationValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	
	public function store($request)
	{
		try {
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
			
			$qualification = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Qualificado cadastrada",
				'data' 	  	=> $qualification,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
			
		}
	}
    
    public function update($request, int $id)
	{
		try {
			$data = $request->all();
			$qualification = $this->repository->find($id);

			if ($data['remove-image'] == '1') {
				if (Storage::disk('s3')->exists($qualification->image)) {
					Storage::delete($qualification->image);
					$data['image'] = null;
				}
			}

			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}

			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$qualification = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Qualificado atualizado",
				'data' 	  	=> $qualification,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	public function delete(int $id)
	{
		try {
			$qualification = $this->repository->find($id);
			
			if (Storage::disk('s3')->exists($qualification->image)) {
				Storage::delete($qualification->image);
			}
			$this->repository->update(array('image' => null), $id);
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Qualificado deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}
}