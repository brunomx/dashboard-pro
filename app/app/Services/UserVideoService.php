<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\UserVideosRepository;
use App\Repositories\VideoRepository;
use App\Validators\UserVideosValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class UserVideoService
{
	private $repository;
	private $repositoryVideo;
	private $validator;
	
	public function __construct(UserVideosRepository $repository, UserVideosValidator $validator, VideoRepository $repositoryVideo)
	{
		$this->repository = $repository;
		$this->repositoryVideo = $repositoryVideo;
		$this->validator  = $validator;
	}

	public function store(array $data, $user)
	{
		try {			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if($data['duration'] > 0){
				$this->repositoryVideo->update(array('duration' => $data['duration']), $data['video_id']);
			}

			$data['user_id'] = $user->id;
			
			$result = $this->repository->findByField(['user_id' => $user->id, 'video_id' => $data['video_id']]);

			if ($result->count() > 0) {
				if($result->first()->completed == '0'){
					if($data['watch_time'] >= $result->first()->watch_time){
						$this->repository->update($data, $result->first()->id);
					}
				}
			} else {
				$this->repository->create($data);
			}
			
			return [
				'success' 	=> true,
				'messages' 	=> "Dados cadastrados",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}
}