<?php

namespace App\Services;

use App\Entities\Video;
use App\Repositories\VideoTagsRepository;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;


class VideoTagsService
{
    private $repository;

    public function __construct(VideoTagsRepository $repository)
    {
        $this->repository = $repository;
    }
    public function store(array $tags, int $video_id)
    {
        try {
            if (!empty($tags)) {
                foreach ($tags as $tag) {
                    $data_tags = array(
                        'video_id' => $video_id,
                        'tag_id' => $tag,
                    );
                    $this->repository->create($data_tags);
                }
            }
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function storeTime(array $tags, array $seconds, int $video_id)
    {
        try {
            if (!empty($tags)) {
                foreach ($tags as $key => $tag) {
                    $data_tags = array(
                        'video_id' => $video_id,
                        'tag_id' => $tag,
                        'seconds' => $seconds[$key],
                        'type' => 1,
                    );
                    $this->repository->create($data_tags);
                }
            }
        } catch (Exception $e) {

            dd($e);
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function update(Request $request, Video $video)
    {
        try {
            $old_tags = $video->permissions->pluck('id')->toArray();
            foreach ($old_tags as $item) {
                if ($request->has('permissions')) {
                    if (in_array($item, $request->get('permissions')) === false) {
                        $this->delete($item);
                    }
                } else {
                    $this->delete($item);
                }
            }

            if ($request->has('permissions')) {
                foreach ($request->get('permissions') as $tag) {
                    if (in_array($tag, $old_tags) === false) {
                        $data_tags = array(
                            'video_id' => $video->id,
                            'tag_id' => $tag,
                        );
                        $this->repository->create($data_tags);
                    }
                }
            }

        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

        }
    }

    public function updateTime(Request $request, Video $video)
    {
        try {
            $old_tags = $video->timeTags->pluck('id')->toArray();

            foreach ($old_tags as $item) {
                if ($request->has('tags')) {
                    if (in_array($item, $request->get('timeTag')) === false) {
                        $this->deleteTimeTag($item);
                    }
                } else {
                    $this->deleteTimeTag($item);
                }
            }

            if ($request->has('timeTag')) {
                $seconds = $request->get('seconds');
                $tags = $request->get('tags');

                foreach ($request->get('timeTag') as $key => $item) {
                    if (in_array($item, $old_tags) === false) {
                        $data_tags = array(
                            'video_id' => $video->id,
                            'tag_id' => $tags[$key],
                            'seconds' => $seconds[$key],
                            'type' => 1,
                        );

                        $this->repository->create($data_tags);
                    } else {
                        if(!is_null($tags[$key]) && !is_null($seconds[$key])){
                            $data_tags = array(
                                'tag_id' => $tags[$key],
                                'seconds' => $seconds[$key],
                            );

                            $this->repository->update($data_tags, $item);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
        }
    }

    public function delete(int $tag_id)
    {
        try {
            $this->repository->deleteWhere(['tag_id' => $tag_id]);
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

        }
    }

    public function deleteTimeTag(int $id)
    {
        try {
            $this->repository->delete($id);
        } catch (Exception $e) {
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

        }
    }
}
