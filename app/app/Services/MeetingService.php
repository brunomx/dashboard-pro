<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\MeetingRepository;
use App\Validators\MeetingValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;
use App\Entities\Meeting;

class MeetingService
{
    private $repository;
	private $validator;

	public function __construct(MeetingRepository $repository, MeetingValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	
	public function store($request)
	{
		try {
			$data = $request->all();
			
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
			
			$meeting = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Reunião cadastrada",
				'data' 	  	=> $meeting,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
			
		}
	}
    
    public function update($request, int $id)
	{
		try {
			$data = $request->all();
			$meeting = $this->repository->find($id);

			if ($data['remove-image'] == '1') {
				if (Storage::disk('s3')->exists($meeting->image)) {
					Storage::delete($meeting->image);
					$data['image'] = null;
				}
			}

			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}

			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$meeting = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Reunião atualizada",
				'data' 	  	=> $meeting,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	public function delete(int $id)
	{
		try {

			$meeting = $this->repository->find($id);
			
			if (Storage::disk('s3')->exists($meeting->image)) {
				Storage::delete($meeting->image);
			}
			$this->repository->update(array('image' => null), $id);
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Reunião deletada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	public function getFromUser($user){
		if (is_null($user->package_id)) {
			$meetings = Meeting::orderBy('date')->where('package_id', null)->get()->filter(function ($item, $key) {
				return $item->diff_days > -5;
			});
		} else {
			$meetings = Meeting::orderBy('date')->where('package_id', null)->orWhere('package_id', '<=', $user->package_id)->get()->filter(function ($item, $key) {
				return $item->diff_days > -5;
			});
		}
	
		return $meetings;
	}
}