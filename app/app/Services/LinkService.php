<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\LinksRepository;
use App\Validators\LinksValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;

class LinkService
{
    private $repository;
	private $validator;

	public function __construct(LinksRepository $repository, LinksValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	public function store($request, $program_id)
	{
		try {
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
			$data['partner_program_id'] = $program_id;
			
			$link = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "link cadastrado",
				'data' 	  	=> $link,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update($request, int $program_id, int $id)
	{
		try {
			$data = $request->all();
			$link = $this->repository->find($id);

			if($data['remove-image'] == '1'){
				if (Storage::disk('local')->exists($link->image)) {
					Storage::delete($link->image);
					$data['image'] = null;
				}
			}

			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}

			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$link = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Link atualizado",
				'data' 	  	=> $link,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Link deletado",
				'data' 	  	=> NULL,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}
}