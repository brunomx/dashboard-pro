<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\PartnerProgramRepository;
use App\Validators\PartnerProgramValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class PartnerProgramService
{
    private $repository;
	private $validator;

	public function __construct(PartnerProgramRepository $repository, PartnerProgramValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	public function store(array $data)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			$program = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Programa cadastrado",
				'data' 	  	=> $program,
			];
		}
		catch(Exception $e)
		{
			dd($e->getMessage());
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update(array $data, int $id)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$program = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Programa atualizado",
				'data' 	  	=> $program,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Programa deletado",
				'data' 	  	=> NULL,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}
}