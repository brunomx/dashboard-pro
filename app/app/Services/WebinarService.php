<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\WebinarRepository;
use App\Validators\WebinarValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class WebinarService
{
    private $repository;
	private $validator;

	public function __construct(WebinarRepository $repository, WebinarValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	public function store(array $data)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			if(!is_null($data['embed'])) {
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
			}

			$webinar = $this->repository->create($data);

			return [
				'success' 	=> true,
				'messages' 	=> "Webinar cadastrado",
				'data' 	  	=> $webinar,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update(array $data, int $id)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			if (!is_null($data['embed'])) {
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
			}

			$webinar = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Webinar atualizado",
				'data' 	  	=> $webinar,
			];
		}
		catch(Exception $e)
		{
			dd($e->getMessage());
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$webinar = $this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Webinar deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}
}