<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\DeviceRepository;
use App\Validators\DeviceValidator;
use Prettus\Validator\Contracts\ValidatorInterface;

class DeviceService
{
    private $repository;
	private $validator;

	public function __construct(DeviceRepository $repository, DeviceValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}

	public function store(array $data, $user)
	{
		try {
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

			$data_device = array(
				'token' => $data['token'],
				'player_ids' => $data['player_ids'],
				'manufacturer' => $data['manufacturer'],
				'model' => $data['model'],
				'platform' => $data['platform'],
				'serial' => $data['serial'],
				'udid' => $data['uuid'],
				'version' => $data['version'],
				'user_id' => $user->id,
			);
			
			$result = $this->repository->findByField(['user_id' => $user->id]);

			if ($result->count() > 0) {
				$device = $this->repository->update($data_device, $result->first()->id);
			} else {
				$device = $this->repository->create($data_device);
			}

			return [
				'success' => true,
				'messages' => "Device cadastrado",
				'data' => $device,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}
}