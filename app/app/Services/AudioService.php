<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\AudioRepository;
use App\Validators\AudioValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;
use App\Entities\Audio;
use App\Entities\UserAudios;

class AudioService
{
    private $repository;
	private $validator;

	public function __construct(AudioRepository $repository, AudioValidator $validator)
	{
		$this->repository 	= $repository;
		$this->validator 	= $validator;
	}
	public function store($request)
	{
		try {
            $data = $request->all();
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

            if ($request->has('path')) {
				$data['path'] = $request->file('path')->store('public');
				$data['size'] = $request->file('path')->getSize();
            }

            if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
            
			$audio = $this->repository->create($data);
            
			return [
				'success' 	=> true,
				'messages' 	=> "Áudio cadastrado",
				'data' 	  	=> $audio,
			];
		}
		catch(Exception $e)
		{
			dd($e->getMessage());
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
    }
    
    public function update($request, int $id)
	{
		try {
            $data = $request->all();
            $audio = $this->repository->find($id);

            if($data['remove-audio'] == '1'){
				if (Storage::disk('local')->exists($audio->path)) {
					Storage::delete($audio->path);
					$data['path'] = null;
					$data['size'] = null;
				}
            }
            
            if($data['remove-image'] == '1'){
				if (Storage::disk('local')->exists($audio->image)) {
					Storage::delete($audio->image);
					$data['image'] = null;
				}
            }
            
            if ($request->has('path')) {
				$data['path'] = $request->file('path')->store('public');
				$data['size'] = $request->file('path')->getSize();
            }

            if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('public');
			}
            
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$audio = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Áudio cadastrado",
				'data' 	  	=> $audio,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function delete(int $id)
	{
		try {
			$audio = $this->repository->find($id);

			if (Storage::disk('local')->exists($audio->path)) {
				Storage::delete($audio->path);
			}

			if (Storage::disk('local')->exists($audio->image)) {
				Storage::delete($audio->image);
			}

            $this->repository->delete($id);
            
			return [
				'success' 	=> true,
				'messages' 	=> "Áudio deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
    }
    
    public function sequence($data)
	{
		try {
			$itens = json_decode($data);
			foreach($itens as $key => $item){
				$this->repository->update(array('sequence' => $key), $item);
			}
			return [
				'success' 	=> true,
				'messages' 	=> "Sequencia atualizada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
			switch (get_class($e)) {
				case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
				case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
				case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
				default:return ['success' => false, 'messages' => get_class($e)];
			}
		}
	}

	public function getFromUser($user){
		if (is_null($user->package_id)) {
			$audios = Audio::orderBy('sequence')->where('package_id', null)->get();
		} else {
			$audios = Audio::orderBy('sequence')->where('package_id', null)->orWhere('package_id', '<=', $user->package_id)->get();
		}

		foreach ($audios as $audio) {
			$result = UserAudios::where(['user_id' => $user->id, 'audio_id' => $audio->id])->get();
			if ($result->count() > 0) {
				$audio->watch_time = $result->first()->watch_time;
				if($result->first()->completed == '1'){
					$audio->progress = 100;
				} else {
					if($audio->watch_time == 0 || $audio->duration == null){
						$audio->progress = 0;
					} else {
						$audio->progress = ($audio->watch_time / $audio->duration) * 100;
					}
				}
			} else {
				$audio->progress = 0;
				$audio->watch_time = 0;
			}
		}

		return $audios;
	}

	public function details($id){
		try {
			$audio = $this->repository->find($id);

			if(is_null($audio->duration)){
				return [
					'success' => true,
					'messages' => "Lista atualizada",
					'data' => [],
				];
			} else {
				$result = array();
				foreach($audio->users()->get() as $item){
					$progress = ($item->watch_time / $audio->duration) * 100;
					$data = array('name' => $item->user->name, 'progress' => $progress, 'completed' => $item->completed);
					array_push($result, $data);
				}
				return [
					'success' 	=> true,
					'messages' 	=> "Lista atualizada",
					'data' 	  	=> $result,
				];
			}
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}
}