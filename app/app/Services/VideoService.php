<?php

namespace App\Services;
use Illuminate\Database\QueryException;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\VideoRepository;
use App\Validators\VideoValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Storage;
use App\Services\VideoTagsService;
use App\Entities\UserVideos;

class VideoService
{
    private $repository;
	private $validator;

	public function __construct(VideoRepository $repository, VideoValidator $validator, VideoTagsService $videoTagsService)
	{
		$this->repository 		= $repository;
		$this->validator 		= $validator;
		$this->videoTagsService = $videoTagsService;
	}

	public function store($request, $course_id)
	{
		try {
			$data = $request->all();
			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
			
			if ($request->has('audio')) {
				$data['audio'] = $request->file('audio')->store('public');
			}

			if ($request->has('embed')) {
				$data_vimeo = $this->getDataVimeo($data['embed']);

				$data['thumbnail'] =  $data_vimeo->thumbnail_url;
				$data['duration'] =  $data_vimeo->duration;
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
			}

			if (!is_null($data['url'])) {
				$data['size'] = $this->getSizeFromUrl($data['url']);
			}

			$data['course_id'] = $course_id;

			$video = $this->repository->create($data);

			// if ($request->has('permissions')) {
			// 	$this->videoTagsService->store($data['permissions'], $video->id);
			// }

			// if (!is_null($data['seconds'][0]) && !is_null($data['tags'][0])) {
			// 	$this->videoTagsService->storeTime($data['tags'], $data['seconds'], $video->id);
			// }

			return [
				'success' 	=> true,
				'messages' 	=> "Video cadastrado",
				'data' 	  	=> $video,
			];
		}
		catch(Exception $e)
		{
			dd($e);
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
    }
    
    public function update($request, int $course_id, int $id)
	{
		try {
			$data = $request->all();
			$video = $this->repository->find($id);
			if($data['remove-audio'] == '1'){
				if (Storage::disk('local')->exists($video->audio)) {
					Storage::delete($video->audio);
					$data['audio'] = null;
				}
			}

			if ($request->has('audio')) {
				$data['audio'] = $request->file('audio')->store('public');
			}

			if ($request->has('embed')) {
				$data_vimeo = $this->getDataVimeo($data['embed']);

				$data['thumbnail'] = $data_vimeo->thumbnail_url;
				$data['duration'] = $data_vimeo->duration;
				$data['embed'] = @str_replace('width="640"', 'width="100%"', $data['embed']);
				$data['embed'] = @str_replace('height="360"', 'height="auto"', $data['embed']);
			}

			if (!is_null($data['url'])) {
    			$data['size'] = $this->getSizeFromUrl($data['url']);
			}

			// if($video->permissions->count() > 0){
			// 	$this->videoTagsService->update($request, $video);
			// } else {
			// 	if ($request->has('permissions')) {
			// 		$this->videoTagsService->store($data['permissions'], $id);
			// 	}
			// }

			// if($video->timeTags->count() > 0){
			// 	$this->videoTagsService->updateTime($request, $video);
			// } else {
			// 	if (!is_null($data['seconds'][0]) && !is_null($data['tags'][0])) {
			// 		$this->videoTagsService->storeTime($data['tags'], $data['seconds'], $id);
			// 	}
			// }

			if (!is_null($data['url'])) {
				$data['size'] = $this->getSizeFromUrl($data['url']);
			}

			$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
			$video = $this->repository->update($data, $id);

			return [
				'success' 	=> true,
				'messages' 	=> "Vídeo atualizado",
				'data' 	  	=> $video,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	public function delete(int $id)
	{
		try {
			$video = $this->repository->find($id);
			if (Storage::disk('local')->exists($video->audio)) {
				Storage::delete($video->audio);
			}

			$this->repository->delete($id);

			return [
				'success' 	=> true,
				'messages' 	=> "Vídeo deletado",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function sequence($data)
	{
		try {
			$itens = json_decode($data['itens']);
			foreach($itens as $key => $item){
				$data_update = [
					'sequence' => $key,
					'course_id' => $data['course_id']
				];
				$this->repository->update($data_update, $item);
			}
			return [
				'success' 	=> true,
				'messages' 	=> "Sequencia atualizada",
				'data' 	  	=> null,
			];
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }

		}
	}

	public function details($id){
		try {
			$video = $this->repository->find($id);
			if(is_null($video->duration)){
				return [
					'success' => true,
					'messages' => "Lista atualizada",
					'data' => [],
				];
			} else {
				$users = UserVideos::orderByDesc('watch_time')->whereRaw("video_id = $id AND ((watch_time <> $video->duration AND completed = 0) OR completed = 1)")->get();
				$result = array();
				foreach($users as $item){
					$progress = ($item->watch_time / $video->duration) * 100;
					$data = array('name' => $item->user->name, 'progress' => $progress, 'completed' => $item->completed);
					array_push($result, $data);
				}
				return [
					'success' 	=> true,
					'messages' 	=> "Lista atualizada",
					'data' 	  	=> $result,
				];
			}
		}
		catch(Exception $e)
		{
            switch (get_class($e)) {
                case QueryException::class:return ['success' => false, 'messages' => $e->getMessage()];
                case ValidatorException::class:return ['success' => false, 'messages' => $e->getMessageBag()];
                case Exception::class:return ['success' => false, 'messages' => $e->getMessage()];
                default:return ['success' => false, 'messages' => get_class($e)];
            }
		}
	}

	private function getDataVimeo($embed){
		preg_match('/src="([^"]+)"/', $embed, $match);
		$url = $match[1];

		$url = explode('/', $url);

		$imgid = $url[4];

		$retorno = json_decode(file_get_contents("https://vimeo.com/api/oembed.json?url=https://vimeo.com/$imgid"));
		return $retorno;
	}

	private function getSizeFromUrl($url){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
		curl_exec($ch);
		$filesize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
		curl_close($ch);

		return $filesize;
	}
}