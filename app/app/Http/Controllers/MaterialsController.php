<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MaterialCreateRequest;
use App\Http\Requests\MaterialUpdateRequest;
use App\Repositories\MaterialRepository;
use App\Validators\MaterialValidator;
use App\Entities\Material;
use App\Entities\MaterialCategory;
use App\Entities\Package;
use App\Services\MaterialService;

class MaterialsController extends Controller
{
    
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(MaterialRepository $repository, MaterialValidator $validator, MaterialService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {   
        if (request()->wantsJson()) {
            $user = request()->user();
            $materials = $this->service->getFromUser($user);

            foreach ($materials as $item) {
                $item->category_name = is_null($item->category) ? '' : $item->category->name;
                if($item->type == 2){
                    $item->thumbnail = $item->path;
                } else {
                    $item->path = $item->path_link;
                }
            }

            return response()->json([
                'materials' => $materials,
                'status' => true,
            ]);
        } else {
            $materials = Material::orderBy('sequence')->get();
            return view('materials.index', ['materials' => $materials]);
        }
    }

    public function create()
    {
        $material = new Material();
        $categories = MaterialCategory::orderBy('name')->get()->pluck('name', 'id');
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');

        return view('materials.create', ['categories' => $categories, 'material' => $material, 'packages' => $packages]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);
        $tag = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('material.index');
    }

    public function edit($id)
    {
        $material = Material::find($id);
        $categories = MaterialCategory::orderBy('name')->get()->pluck('name', 'id');
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');

        return view('materials.edit', ['material' => $material, 'categories' => $categories, 'packages' => $packages]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('material.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('material.index');
    }

    public function sequence(Request $request){
        $request = $this->service->sequence($request->get('itens'));

        return response()->json($request);
    }
}
