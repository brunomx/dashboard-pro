<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TagCategoryCreateRequest;
use App\Http\Requests\TagCategoryUpdateRequest;
use App\Repositories\TagCategoryRepository;
use App\Validators\TagCategoryValidator;
use App\Entities\TagCategory;
use App\Services\TagCategoryService;

class TagCategoriesController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(TagCategoryRepository $repository, TagCategoryValidator $validator, TagCategoryService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }


    public function index()
    {
        return view('tags.categories.index', ['categories' => TagCategory::orderBy('name')->get()]);
    }

    public function create()
    {
        return view('tags.categories.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());
        $tag = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('tag-category.index');
    }

    public function edit($id)
    {
        $category = TagCategory::find($id);
        return view('tags.categories.edit', ['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('tag-category.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('tag-category.index');
    }

}
