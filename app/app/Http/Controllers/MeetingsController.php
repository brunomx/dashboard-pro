<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MeetingCreateRequest;
use App\Http\Requests\MeetingUpdateRequest;
use App\Repositories\MeetingRepository;
use App\Validators\MeetingValidator;
use App\Services\MeetingService;
use App\Entities\Meeting;
use App\Entities\Package;

class MeetingsController extends Controller
{

    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(MeetingRepository $repository, MeetingValidator $validator, MeetingService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    public function index()
    {   
        if (request()->wantsJson()) {
            $user = request()->user();
            $meetings = $this->service->getFromUser($user);

            $data = array();
            foreach ($meetings as $item) {
                $item->date = $item->formatted_date;
                if(!is_null($item->image)){
                    $item->image = $item->image_link;
                }

                array_push($data, $item);
            }
            return response()->json([
                'meetings' => $data,
                'status' => true,
            ]);
        } else {
            $meetings = Meeting::orderByDesc('date')->get();
            return view('meetings.index', ['meetings' => $meetings]);
        }
    }

    public function create()
    {
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        return view('meetings.create', ['packages' => $packages]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('meeting.index');
    }

    public function edit($id)
    {
        $meeting = Meeting::find($id);
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');

        return view('meetings.edit', ['meeting' => $meeting, 'packages' => $packages]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('meeting.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('meeting.index');
    }
}
