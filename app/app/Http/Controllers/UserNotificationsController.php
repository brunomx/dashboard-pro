<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserNotificationsCreateRequest;
use App\Http\Requests\UserNotificationsUpdateRequest;
use App\Repositories\UserNotificationsRepository;
use App\Validators\UserNotificationsValidator;

/**
 * Class UserNotificationsController.
 *
 * @package namespace App\Http\Controllers;
 */
class UserNotificationsController extends Controller
{
    /**
     * @var UserNotificationsRepository
     */
    protected $repository;

    /**
     * @var UserNotificationsValidator
     */
    protected $validator;

    /**
     * UserNotificationsController constructor.
     *
     * @param UserNotificationsRepository $repository
     * @param UserNotificationsValidator $validator
     */
    public function __construct(UserNotificationsRepository $repository, UserNotificationsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $userNotifications = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userNotifications,
            ]);
        }

        return view('userNotifications.index', compact('userNotifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserNotificationsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserNotificationsCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $userNotification = $this->repository->create($request->all());

            $response = [
                'message' => 'UserNotifications created.',
                'data'    => $userNotification->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userNotification = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $userNotification,
            ]);
        }

        return view('userNotifications.show', compact('userNotification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userNotification = $this->repository->find($id);

        return view('userNotifications.edit', compact('userNotification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserNotificationsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserNotificationsUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $userNotification = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'UserNotifications updated.',
                'data'    => $userNotification->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'UserNotifications deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'UserNotifications deleted.');
    }
}
