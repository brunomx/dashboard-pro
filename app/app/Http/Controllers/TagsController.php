<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\TagCreateRequest;
use App\Http\Requests\TagUpdateRequest;
use App\Validators\TagValidator;
use App\Repositories\TagRepository;
use App\Entities\Tag;
use App\Entities\TagCategory;
use App\Services\TagService;

class TagsController extends Controller
{

    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(TagRepository $repository, TagValidator $validator, TagService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('tags.index', ['tags' => Tag::orderBy('name')->get()]);
    }

    public function create()
    {
        $categories = TagCategory::orderBy('name')->get()->pluck('name', 'id');
        return view('tags.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('tag.index');
    }

    public function edit($id)
    {
        $tag = Tag::find($id);
        $categories = TagCategory::orderBy('name')->get()->pluck('name', 'id');

        return view('tags.edit', ['tag' => $tag, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('tag.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('tag.index');
    }
}
