<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\NotificationCreateRequest;
use App\Http\Requests\NotificationUpdateRequest;
use App\Validators\NotificationValidator;
use App\Repositories\NotificationRepository;
use App\Entities\Notification;
use App\Entities\UserNotifications;
use App\Entities\Device;
use App\Entities\Package;
use App\Services\NotificationService;

class NotificationsController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(NotificationRepository $repository, NotificationValidator $validator, NotificationService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {   
        if (request()->wantsJson()) {
            $user = request()->user();
            $notifications = $this->service->getFromUser($user->id);

            $data = $notifications->toArray();

            return response()->json([
                'notifications' => $data,
                'status' => true,
            ]);
        } else {
            $notifications = Notification::where('type', 0)->orderByDesc('created_at')->get();
            return view('notifications.index', ['notifications' => $notifications]);
        }
    }

    public function create()
    {
        $users = Device::with('user')->get()->where('user', '<>' ,null)->pluck('user.name', 'user.id');
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');

        $notification = new Notification();

        return view('notifications.create', ['notification' => $notification, 'users' => $users, 'packages' => $packages]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('notification.index');
    }

    public function show($id)
    {
        $notification = $this->service->getDetails((int) $id);
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');

        return view('notifications.show', ['notification' => $notification, 'packages' => $packages]);
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('notification.index');
    }

    public function updateStatus(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->updateStatus($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }
}