<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CourseCreateRequest;
use App\Http\Requests\CourseUpdateRequest;
use App\Repositories\CourseRepository;
use App\Validators\CourseValidator;
use App\Entities\Course;
use App\Entities\Tag;
use App\Entities\CourseTags;
use App\Entities\Package;
use App\Services\CourseService;

class CoursesController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(CourseRepository $repository, CourseValidator $validator, CourseService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service  = $service;
    }

    public function index()
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $courses = $this->service->getFromUser($user);
            foreach ($courses as  $item) {
                $item->image = $item->image_link;
            }
            return response()->json([
                'courses' => $courses,
                'status' => true,
            ]);
        } else {
            $courses = Course::orderBy('sequence')->where('type', 0)->get();
            return view('courses.index', ['courses' => $courses]);
        }
    }

    public function create()
    {
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        return view('courses.create', ['packages' => $packages]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('course.index');
    }

    public function edit($id)
    {
        $course = Course::find($id);
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        
        return view('courses.edit', ['course' => $course, 'packages' => $packages]);
    }

    public function show($id)
    {
        $course = Course::find($id);
        if (request()->wantsJson()) {
            $user = request()->user();

            $course->videos = $this->service->getVideosFromUser($user, $course);

            return response()->json([
                'data' => $course,
                'status' => true,
            ]);
        } else {
            $tags = Tag::orderBy('name')->get()->pluck('name', 'id');
            $tags_selected = $course->tags->pluck('id');

            return view('courses.edit', ['course' => $course, 'tags' => $tags, 'tags_selected' => $tags_selected]);
        }
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('course.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('course.index');
    }

    public function sequence(Request $request){
        $request = $this->service->sequence($request->get('itens'));

        return response()->json($request);
    }
}
