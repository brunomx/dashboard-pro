<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserVideosCreateRequest;
use App\Http\Requests\UserVideosUpdateRequest;
use App\Repositories\UserVideosRepository;
use App\Services\UserVideoService;

class UserVideosController extends Controller
{

    protected $repository;
    protected $service;

    public function __construct(UserVideosRepository $repository, UserVideoService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->wantsJson()) {
        }
    }

    public function register(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->store($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }
}
