<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use App\Validators\UserValidator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Repositories\UserRepository;
use Auth;
use Carbon\Carbon;
use App\Entities\User;
use App\Services\UserService;
use Laravel\Passport\Passport;


class Controller extends BaseController
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(UserRepository $repository,  UserValidator $validator, UserService $service)
    {
        $this->validator = $validator;
        $this->service = $service;
    }
    
    public function index()
    {
        if (Auth::check() === false) {
            return view('user.login');
        } else {
            return redirect()->route('audio.index');
        }
    }
    
    public function login(){
        if (Auth::check() === false) {
            return view('user.login');
        } else {
            return redirect()->route('dashboard');
        }
    }
    
    public function logout(){
        if (Auth::check() === true) {
            Auth::logout();
            return redirect()->route('dashboard');
        } else {
            return view('user.login', ['user' => Auth::user()]);
        }
    }
    
    public function auth(Request $request){
        $data = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'permission' => 'app.admin',
        ];
        
        if (Auth::attempt($data, false)) {
            return redirect()->route('audio.index');
        } else {
            session()->flash('success', [
            'success' => false,
            'messages' => 'Dados incorretos',
            ]);
            
            return redirect()->route('user.login');
        }
    }
 
    public function authApp(Request $request){
        if (request()->wantsJson()) {
            $request->validate([
                'username' => 'required|string',
                'password' => 'required|string',
            ]);
    
            $credentials = request(['username', 'password']);
    
            if (!Auth::attempt($credentials)) {
                $result = $this->service->loginXpert($request->all());
                if($result['success'] == false){
                    return response()->json([
                        'status' => false,
                        'message' => 'Unauthorized',
                    ], 401);
                }

                $user = $result['data'];
            } else {
                $user = $request->user();
                $this->service->checkPackage($request->all(), $user);
            }
            foreach ($user->tokens as $token) {
                $token->revoke();
            }
    
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
    
            $token->save();
            return response()->json([
                'status' => true,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'user' => $user,
            ]);
        }
    }

    public function packageUpdate(Request $request){
        $result = $this->service->packageUpdate($request->all());
        if($result['success'] == false){
            return response()->json([
                'status' => false,
                'messages' => $result['messages'],
            ]);
        } else {
            return response()->json($result);
        }
    }

    public function share($username = null){
        return view('user.share', ['username' => $username]);
    }
}
