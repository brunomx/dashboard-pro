<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\LinksCreateRequest;
use App\Http\Requests\LinksUpdateRequest;
use App\Repositories\LinksRepository;
use App\Validators\LinksValidator;
use App\Services\LinkService;
use App\Entities\PartnerProgram;

/**
 * Class LinksController.
 *
 * @package namespace App\Http\Controllers;
 */
class LinksController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(LinksRepository $repository, LinksValidator $validator, LinkService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function list($program_id)
    {
        $links = PartnerProgram::find($program_id)->links;
        return view('programs.links.index', ['links' => $links, 'program_id' => $program_id]);
    }

    public function create($program_id)
    {
        return view('programs.links.create', ['program_id' => $program_id]);
    }

    public function store(Request $request, $program_id)
    {
        $request = $this->service->store($request, $program_id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('program.link.index', ['id' => $program_id]);
    }

    public function edit($program_id, $id)
    {
        $link = $this->repository->find($id);

        return view('programs.links.edit', ['link' => $link, 'program_id' => $program_id]);
    }

    public function update(Request $request, $program_id, $id)
    {
        $request = $this->service->update($request, (int) $program_id, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('program.link.index', ['id' => $program_id]);
    }

    public function remove(Request $request)
    {
        $response = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $response['success'],
            'messages'  => $response['messages']
        ]);

        return redirect()->route('program.link.index', ['id' => $request->get('entity_id')]);
    }    
}
