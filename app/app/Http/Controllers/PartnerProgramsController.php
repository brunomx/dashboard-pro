<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PartnerProgramCreateRequest;
use App\Http\Requests\PartnerProgramUpdateRequest;
use App\Repositories\PartnerProgramRepository;
use App\Validators\PartnerProgramValidator;
use App\Entities\PartnerProgram;
use App\Services\PartnerProgramService;

/**
 * Class PartnerProgramsController.
 *
 * @package namespace App\Http\Controllers;
 */
class PartnerProgramsController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(PartnerProgramRepository $repository, PartnerProgramValidator $validator, PartnerProgramService $service)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('programs.index', ['programs' => PartnerProgram::orderBy('created_at')->get()]);
    }

    public function create()
    {
        return view('programs.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('program.index');
    }

    public function edit($id)
    {
        $program = PartnerProgram::find($id);

        return view('programs.edit', ['program' => $program]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('program.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('program.index');
    }
    
}
