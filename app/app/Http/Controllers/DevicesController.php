<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DeviceCreateRequest;
use App\Http\Requests\DeviceUpdateRequest;
use App\Repositories\DeviceRepository;
use App\Validators\DeviceValidator;
use App\Entities\Device;
use App\Services\DeviceService;


class DevicesController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(DeviceRepository $repository, DeviceValidator $validator, DeviceService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $devices = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $devices,
            ]);
        }

        return view('devices.index', compact('devices'));
    }

    public function device(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->store($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }

}
