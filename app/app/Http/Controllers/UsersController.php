<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;
use App\Entities\User;

class UsersController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(UserRepository $repository, UserValidator $validator, UserService $service)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
    }

    public function index()
    {

        $users = $this->service->getUsersPackExpired();
        dd($users);
        $users = User::orderBy('first_name')->where('id', '<>', Auth::user()->id)->where('permission', 'app.admin')->get();
        return view('user.index', ['users' => $users]);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('user.index');
    }

    public function profile()
    {
        $user = Auth::user();
        return view('user.edit', ['user' => $user, 'current' => true]);
    }

    public function password()
    {
        return view('user.password');
    }

    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit', ['user' => $user, 'current' => false]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        if (Auth::user()->id == $id) {
            return redirect()->route('audio.index');
        } else {
            return redirect()->route('user.index');
        }
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('user.index');
    }

    public function changePassword(Request $request)
    {
        if (!Hash::check($request->get('current'), Auth::user()->password)) {
            session()->flash('success', [
                'success' => false,
                'messages' => 'Senha atual incorreta',
            ]);
            return redirect()->route('user.password');
        } else {
           $request->validate([
                'new_password' => 'required|min:4',
                'confirm_password' => 'required|same:new_password',
            ]);

            $request = $this->service->password($request->get('new_password'), (int) Auth::user()->id);
            session()->flash('success', [
                'success' => $request['success'],
                'messages' => $request['messages'],
            ]);

            return redirect()->route('audio.index');
        }
    }
}
