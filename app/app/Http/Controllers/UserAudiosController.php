<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UserAudiosCreateRequest;
use App\Http\Requests\UserAudiosUpdateRequest;
use App\Repositories\UserAudiosRepository;
use App\Services\UserAudioService;

class UserAudiosController extends Controller
{

    protected $repository;
    protected $service;

    public function __construct(UserAudiosRepository $repository, UserAudioService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->wantsJson()) {
        }
    }

    public function register(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->store($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }
}
