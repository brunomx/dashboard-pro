<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\QualificationCreateRequest;
use App\Http\Requests\QualificationUpdateRequest;
use App\Repositories\QualificationRepository;
use App\Validators\QualificationValidator;
use App\Services\QualificationService;
use App\Entities\Qualification;

class QualificationsController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(QualificationRepository $repository, QualificationValidator $validator, QualificationService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    public function index()
    {
        if (request()->wantsJson()) {
            $qualifications = Qualification::orderBy('month')->get();
            foreach ($qualifications as $item) {
                $item->level_name = $item->level_name;
                if(!is_null($item->image)){
                    $item->image = $item->image_link;
                }
            }
            return response()->json([
                'qualifications' => $qualifications,
                'status' => true,
            ]);
        } else {
            $qualifications = Qualification::orderByDesc('created_at')->get();
            return view('qualifications.index', ['qualifications' => $qualifications]);
        }
    }

    public function create()
    {
        $qualification = new Qualification();
        return view('qualifications.create', ['qualification' => $qualification]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('qualification.index');
    }

    public function edit($id)
    {
        $qualification = Qualification::find($id);
        
        return view('qualifications.edit', ['qualification' => $qualification]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('qualification.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('qualification.index');
    }
}
