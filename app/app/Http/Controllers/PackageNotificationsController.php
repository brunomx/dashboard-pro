<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PackageNotificationsCreateRequest;
use App\Http\Requests\PackageNotificationsUpdateRequest;
use App\Repositories\PackageNotificationsRepository;
use App\Validators\PackageNotificationsValidator;
use App\Services\PackageNotificationsService;
use App\Entities\PackageNotifications;

class PackageNotificationsController extends Controller
{

    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(PackageNotificationsRepository $repository, PackageNotificationsValidator $validator, PackageNotificationsService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    public function index()
    {
        $notifications = PackageNotifications::orderByDesc('days')->get();
        return view('packages.index', ['notifications' => $notifications]);
    }

    public function create()
    {
        $notification = new PackageNotifications(); 
        return view('packages.create', ['notification' => $notification]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('package.index');
    }

    public function edit($id)
    {
        $notification = PackageNotifications::find($id);
        return view('packages.edit', ['notification' => $notification]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('package.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('package.index');
    }
}
