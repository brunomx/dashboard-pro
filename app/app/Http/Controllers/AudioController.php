<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\AudioCreateRequest;
use App\Http\Requests\AudioUpdateRequest;
use App\Repositories\AudioRepository;
use App\Validators\AudioValidator;
use App\Entities\Audio;
use App\Entities\Package;
use App\Services\AudioService;

class AudioController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(AudioRepository $repository, AudioValidator $validator, AudioService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $audios = $this->service->getFromUser($user);
            foreach ($audios as $item) {
                if(!is_null($item->image)){
                    $item->image = $item->image_link;
                }
                $item->path = $item->path_link;
            }

            return response()->json([
                'audios' => $audios,
                'status' => true,
            ]);
        } else {
            $audios = Audio::orderBy('sequence')->get();
            return view('audios.index', ['audios' => $audios]);
        }
    }

    public function create()
    {
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        return view('audios.create', ['packages' => $packages]);
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('audio.index');
    }

    public function edit($id)
    {
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        $audio = Audio::find($id);

        return view('audios.edit', ['audio' => $audio, 'packages' => $packages]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('audio.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('audio.index');
    }

    public function sequence(Request $request){
        $request = $this->service->sequence($request->get('itens'));

        return response()->json($request);
    }

    public function details(Request $request){        
        try {
            $request = $this->service->details($request->get('id'));

            return response()->json([
                'success' => $request['success'],
                'messages' => $request['messages'],
                'data' => $request['data'],
            ]);            
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'materials' => $e->getMessage(),
            ]);
        }
    }
    
}
