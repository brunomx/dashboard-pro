<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MaterialCategoryCreateRequest;
use App\Http\Requests\MaterialCategoryUpdateRequest;
use App\Repositories\MaterialCategoryRepository;
use App\Validators\MaterialCategoryValidator;
use App\Entities\MaterialCategory;
use App\Services\MaterialCategoryService;

class MaterialCategoriesController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(MaterialCategoryRepository $repository, MaterialCategoryValidator $validator, MaterialCategoryService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('materials.categories.index', ['categories' => MaterialCategory::orderBy('name')->get()]);
    }

    public function create()
    {
        return view('materials.categories.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());
        $tag = $request['success'] ? $request['data'] : null;

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('material-category.index');
    }

    public function edit($id)
    {
        $category = MaterialCategory::find($id);
        return view('materials.categories.edit', ['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('material-category.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('material-category.index');
    }
}
