<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ContactCreateRequest;
use App\Http\Requests\ContactUpdateRequest;
use App\Validators\ContactValidator;
use App\Repositories\ContactRepository;
use App\Services\ContactService;

class ContactsController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(ContactRepository $repository,  ContactValidator $validator, ContactService $service)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->service = $service;
        $this->middleware('auth');
    }

    public function index()
    {   
        if (request()->wantsJson()) {
            $user = request()->user();
            $contacts = $this->repository->findWhere(['user_id' => $user->id, 'status' => 0]);

            foreach ($contacts as $item) {
                $item->stars = $item->stars;
                $item->invitation = $item->formatted_invitation;
                $item->plan = $item->formatted_plan;
                $item->fup = $item->formatted_fup;
                $item->closing = $item->formatted_closing;
            }

            return response()->json([
                'contacts' => $contacts->toArray(),
                'status' => true,
            ]);
        }
    }

    public function importList()
    {   
        if (request()->wantsJson()) {
            $user = request()->user();
            $contacts = $this->repository->findWhere(['user_id' => $user->id, 'status' => 1]);

            return response()->json([
                'contacts' => $contacts->toArray(),
                'status' => true,
            ]);
        }
    }

    public function register(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->store($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }

    public function importRegister(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->storeImport($request->all(), $user);
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
                'data' => $request['data'],
            ]);
        }
    }

    public function remove(Request $request)
    {
        if (request()->wantsJson()) {
            $user = request()->user();
            $request = $this->service->delete($request->get('contact_id'));
            return response()->json([
                'message' => $request['messages'],
                'status' => $request['success'],
            ]);
        }
    }
}
