<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\VideoCreateRequest;
use App\Http\Requests\VideoUpdateRequest;
use App\Repositories\VideoRepository;
use App\Validators\VideoValidator;
use App\Entities\Video;
use App\Entities\Tag;
use App\Entities\Package;
use App\Services\VideoService;

class VideosController extends Controller
{
    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(VideoRepository $repository, VideoValidator $validator, VideoService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    public function list($course_id)
    {
        $videos = Video::orderBy('sequence')->where('course_id', $course_id)->get();

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $videos,
                'course_id' => $course_id,
                'status' => true,
            ]);
        } else {
            return view('courses.videos.index', ['videos' => $videos, 'course_id' => $course_id]);
        }
    }

    public function create($course_id)
    {
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        $before_list = Video::orderBy('sequence')->where('course_id', $course_id)->get()->pluck('name', 'id');

        return view('courses.videos.create', ['course_id' => $course_id, 'packages' => $packages, 'before_list' => $before_list]);
    }

    public function store(Request $request, $course_id)
    {
        $request = $this->service->store($request, $course_id);

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('course.video.index', ['id' => $course_id]);
    }

    public function edit($course_id, $id)
    {
        $video = Video::find($id);
        $packages = Package::orderBy('id')->get()->pluck('name', 'id');
        $before_list = Video::orderBy('sequence')->where('course_id', $course_id)->where('id', '<>', $id)->get()->pluck('name', 'id');

        return view('courses.videos.edit', ['video' => $video, 'packages' => $packages, 'course_id' => $course_id, 'before_list' => $before_list]);
    }

    public function update(Request $request, $course_id, $id)
    {
        $request = $this->service->update($request, (int) $course_id, (int) $id);

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('course.video.index', ['id' => $course_id]);
    }

    public function remove(Request $request)
    {
        $response = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $response['success'],
            'messages'  => $response['messages']
        ]);

        return redirect()->route('course.video.index', ['id' => $request->get('entity_id')]);
    }

    public function sequence(Request $request){
        $request = $this->service->sequence($request->all());

        return response()->json($request);
    }

    public function details(Request $request){        
        try {
            $request = $this->service->details($request->get('id'));

            return response()->json([
                'success' => $request['success'],
                'messages' => $request['messages'],
                'data' => $request['data'],
            ]);            
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'materials' => $e->getMessage(),
            ]);
        }
    }
}
