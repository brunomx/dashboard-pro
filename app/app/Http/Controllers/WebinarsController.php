<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\WebinarCreateRequest;
use App\Http\Requests\WebinarUpdateRequest;
use App\Repositories\WebinarRepository;
use App\Validators\WebinarValidator;
use \App\Entities\Webinar;
use App\Services\WebinarService;

class WebinarsController extends Controller
{

    protected $repository;
    protected $validator;
    protected $service;

    public function __construct(WebinarRepository $repository, WebinarValidator $validator, WebinarService $service)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->service = $service;
    }

    public function index()
    {
        $webinars = Webinar::orderBy('name')->get();

        if (request()->wantsJson()) {
            return response()->json([
                'webinars' => $webinars,
                'status' => true,
            ]);
        } else {
            return view('webinars.index', ['webinars' => $webinars]);
        }
    }

    public function create()
    {
        return view('webinars.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        session()->flash('success', [
            'success' => $request['success'],
            'messages' => $request['messages'],
        ]);

        return redirect()->route('webinar.index');
    }

    public function edit($id)
    {
        $webinar = Webinar::find($id);

        return view('webinars.edit', ['webinar' => $webinar]);
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), (int) $id);
        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('webinar.index');
    }

    public function remove(Request $request)
    {
        $request = $this->service->delete($request->get('id'));

        session()->flash('success', [
            'success'   => $request['success'],
            'messages'  => $request['messages']
        ]);

        return redirect()->route('webinar.index');
    }

}
