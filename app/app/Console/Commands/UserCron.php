<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;

class UserCron extends Command
{
    protected $signature = 'user:package';
    protected $description = 'Check user date package';
    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    public function handle()
    {
        $result = $this->service->getUsersPackExpired();
        dd($result);
        $this->info('Generated notifications successfully!');
    }
}
