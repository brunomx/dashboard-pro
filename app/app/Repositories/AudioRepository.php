<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AudioRepository.
 *
 * @package namespace App\Repositories;
 */
interface AudioRepository extends RepositoryInterface
{
    //
}
