<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CourseTagsRepository.
 *
 * @package namespace App\Repositories;
 */
interface CourseTagsRepository extends RepositoryInterface
{
    //
}
