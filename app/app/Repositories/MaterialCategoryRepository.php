<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MaterialCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface MaterialCategoryRepository extends RepositoryInterface
{
    //
}
