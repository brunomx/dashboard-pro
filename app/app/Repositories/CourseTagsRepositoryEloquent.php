<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CourseTagsRepository;
use App\Entities\CourseTags;
use App\Validators\CourseTagsValidator;

/**
 * Class CourseTagsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CourseTagsRepositoryEloquent extends BaseRepository implements CourseTagsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CourseTags::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
