<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserVideosRepository;
use App\Entities\UserVideos;
use App\Validators\UserVideosValidator;

/**
 * Class UserVideosRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserVideosRepositoryEloquent extends BaseRepository implements UserVideosRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserVideos::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
