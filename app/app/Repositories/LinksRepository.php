<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LinksRepository.
 *
 * @package namespace App\Repositories;
 */
interface LinksRepository extends RepositoryInterface
{
    //
}
