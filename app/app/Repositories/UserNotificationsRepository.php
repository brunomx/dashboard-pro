<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserNotificationsRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserNotificationsRepository extends RepositoryInterface
{
    //
}
