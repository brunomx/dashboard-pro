<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VideoTagsRepository.
 *
 * @package namespace App\Repositories;
 */
interface VideoTagsRepository extends RepositoryInterface
{
    //
}
