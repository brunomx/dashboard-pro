<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MaterialCategoryRepository;
use App\Entities\MaterialCategory;
use App\Validators\MaterialCategoryValidator;

/**
 * Class MaterialCategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MaterialCategoryRepositoryEloquent extends BaseRepository implements MaterialCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MaterialCategory::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MaterialCategoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
