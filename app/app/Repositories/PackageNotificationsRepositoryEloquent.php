<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PackageNotificationsRepository;
use App\Entities\PackageNotifications;
use App\Validators\PackageNotificationsValidator;

/**
 * Class PackageNotificationsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PackageNotificationsRepositoryEloquent extends BaseRepository implements PackageNotificationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PackageNotifications::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PackageNotificationsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
