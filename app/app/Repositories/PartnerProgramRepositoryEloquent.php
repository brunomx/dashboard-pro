<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PartnerProgramRepository;
use App\Entities\PartnerProgram;
use App\Validators\PartnerProgramValidator;

/**
 * Class PartnerProgramRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PartnerProgramRepositoryEloquent extends BaseRepository implements PartnerProgramRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PartnerProgram::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PartnerProgramValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
