<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserNotificationsRepository;
use App\Entities\UserNotifications;
use App\Validators\UserNotificationsValidator;

/**
 * Class UserNotificationsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserNotificationsRepositoryEloquent extends BaseRepository implements UserNotificationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserNotifications::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return UserNotificationsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
