<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PackageNotificationsRepository.
 *
 * @package namespace App\Repositories;
 */
interface PackageNotificationsRepository extends RepositoryInterface
{
    //
}
