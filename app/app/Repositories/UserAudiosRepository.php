<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserAudiosRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserAudiosRepository extends RepositoryInterface
{
    //
}
