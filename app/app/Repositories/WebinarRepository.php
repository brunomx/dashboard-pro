<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface WebinarRepository.
 *
 * @package namespace App\Repositories;
 */
interface WebinarRepository extends RepositoryInterface
{
    //
}
