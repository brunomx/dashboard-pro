<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\VideoTagsRepository;
use App\Entities\VideoTags;
use App\Validators\VideoTagsValidator;

/**
 * Class VideoTagsRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VideoTagsRepositoryEloquent extends BaseRepository implements VideoTagsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VideoTags::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
