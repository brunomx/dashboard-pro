<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserVideosRepository.
 *
 * @package namespace App\Repositories;
 */
interface UserVideosRepository extends RepositoryInterface
{
    //
}
