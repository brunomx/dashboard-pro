<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PartnerProgramRepository.
 *
 * @package namespace App\Repositories;
 */
interface PartnerProgramRepository extends RepositoryInterface
{
    //
}
