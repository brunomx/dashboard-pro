<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Links;

/**
 * Class LinksTransformer.
 *
 * @package namespace App\Transformers;
 */
class LinksTransformer extends TransformerAbstract
{
    /**
     * Transform the Links entity.
     *
     * @param \App\Entities\Links $model
     *
     * @return array
     */
    public function transform(Links $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
