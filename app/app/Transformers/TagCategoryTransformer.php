<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\TagCategory;

/**
 * Class TagCategoryTransformer.
 *
 * @package namespace App\Transformers;
 */
class TagCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the TagCategory entity.
     *
     * @param \App\Entities\TagCategory $model
     *
     * @return array
     */
    public function transform(TagCategory $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
