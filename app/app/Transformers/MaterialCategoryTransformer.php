<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\MaterialCategory;

/**
 * Class MaterialCategoryTransformer.
 *
 * @package namespace App\Transformers;
 */
class MaterialCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the MaterialCategory entity.
     *
     * @param \App\Entities\MaterialCategory $model
     *
     * @return array
     */
    public function transform(MaterialCategory $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
