<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\UserNotifications;

/**
 * Class UserNotificationsTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserNotificationsTransformer extends TransformerAbstract
{
    /**
     * Transform the UserNotifications entity.
     *
     * @param \App\Entities\UserNotifications $model
     *
     * @return array
     */
    public function transform(UserNotifications $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
