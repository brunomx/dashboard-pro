<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\UserAudios;

/**
 * Class UserAudiosTransformer.
 *
 * @package namespace App\Transformers;
 */
class UserAudiosTransformer extends TransformerAbstract
{
    /**
     * Transform the UserAudios entity.
     *
     * @param \App\Entities\UserAudios $model
     *
     * @return array
     */
    public function transform(UserAudios $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
