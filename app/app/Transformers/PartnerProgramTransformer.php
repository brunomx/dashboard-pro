<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PartnerProgram;

/**
 * Class PartnerProgramTransformer.
 *
 * @package namespace App\Transformers;
 */
class PartnerProgramTransformer extends TransformerAbstract
{
    /**
     * Transform the PartnerProgram entity.
     *
     * @param \App\Entities\PartnerProgram $model
     *
     * @return array
     */
    public function transform(PartnerProgram $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
