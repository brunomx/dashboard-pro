<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Audio;

/**
 * Class AudioTransformer.
 *
 * @package namespace App\Transformers;
 */
class AudioTransformer extends TransformerAbstract
{
    /**
     * Transform the Audio entity.
     *
     * @param \App\Entities\Audio $model
     *
     * @return array
     */
    public function transform(Audio $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
