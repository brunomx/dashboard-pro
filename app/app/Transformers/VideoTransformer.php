<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Video;

/**
 * Class VideoTransformer.
 *
 * @package namespace App\Transformers;
 */
class VideoTransformer extends TransformerAbstract
{
    /**
     * Transform the Video entity.
     *
     * @param \App\Entities\Video $model
     *
     * @return array
     */
    public function transform(Video $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
