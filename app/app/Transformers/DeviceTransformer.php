<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Device;

/**
 * Class DeviceTransformer.
 *
 * @package namespace App\Transformers;
 */
class DeviceTransformer extends TransformerAbstract
{
    /**
     * Transform the Device entity.
     *
     * @param \App\Entities\Device $model
     *
     * @return array
     */
    public function transform(Device $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
