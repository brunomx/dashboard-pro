<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\PackageNotifications;

/**
 * Class PackageNotificationsTransformer.
 *
 * @package namespace App\Transformers;
 */
class PackageNotificationsTransformer extends TransformerAbstract
{
    /**
     * Transform the PackageNotifications entity.
     *
     * @param \App\Entities\PackageNotifications $model
     *
     * @return array
     */
    public function transform(PackageNotifications $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
