<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Webinar;

/**
 * Class WebinarTransformer.
 *
 * @package namespace App\Transformers;
 */
class WebinarTransformer extends TransformerAbstract
{
    /**
     * Transform the Webinar entity.
     *
     * @param \App\Entities\Webinar $model
     *
     * @return array
     */
    public function transform(Webinar $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
