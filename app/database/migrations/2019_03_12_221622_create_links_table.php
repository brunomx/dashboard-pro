<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLinksTable.
 */
class CreateLinksTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links', function(Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('code');
			$table->text('link')->nullable();
			$table->text('image')->nullable();
			$table->unsignedInteger('partner_program_id')->nullable();

			$table->foreign('partner_program_id')->references('id')->on('partner_programs');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('links', function (Blueprint $table) {
			$table->dropForeign('links_partner_program_id_foreign');
		});

		Schema::drop('links');
	}
}
