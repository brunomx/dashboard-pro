<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreatePackageNotificationsTable.
 */
class CreatePackageNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_notifications', function(Blueprint $table) {
			$table->increments('id');
			
			$table->string('title');
			$table->string('subtitle')->nullable();
			$table->text('message');
			$table->char('before', 1)->default(1);
			$table->integer('days')->default(0);

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('package_notifications');
	}
}
