<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableMeetingCoordenates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->string('address_address')->nullable()->after('link');
            $table->double('address_latitude')->nullable()->after('address_address');
            $table->double('address_longitude')->nullable()->after('address_latitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->dropColumn('address_address');
            $table->dropColumn('address_latitude');
            $table->dropColumn('address_longitude');
        });
    }
}
