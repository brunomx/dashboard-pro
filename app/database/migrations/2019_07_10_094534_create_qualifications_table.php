<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateQualificationsTable.
 */
class CreateQualificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('qualifications', function(Blueprint $table) {
			$table->increments('id');

			$table->string('name')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->integer('level')->nullable();
			$table->integer('month')->nullable();
			$table->integer('year')->nullable();
			$table->text('image')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('qualifications');
	}
}
