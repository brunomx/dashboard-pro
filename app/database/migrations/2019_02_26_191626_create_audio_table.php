<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAudioTable.
 */
class CreateAudioTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audio', function(Blueprint $table) {
			$table->increments('id');
   			$table->string('name');
			$table->text('url')->nullable();
			$table->char('type', 1)->default(0);
   			$table->text('path')->nullable();
			$table->text('image')->nullable();
			$table->integer('sequence')->nullable();
			
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audio');
	}
}
