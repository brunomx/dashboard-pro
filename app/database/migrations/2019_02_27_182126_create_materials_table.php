<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateMaterialsTable.
 */
class CreateMaterialsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('materials', function(Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('path')->nullable();
			$table->string('embed')->nullable();
			$table->text('url')->nullable();
			$table->bigInteger('size')->nullable();
			$table->integer('sequence')->nullable();
			$table->dateTime('publication')->nullable();
			$table->char('type', 1)->default(0);
			$table->unsignedInteger('category_id')->nullable();

			$table->foreign('category_id')->references('id')->on('material_categories');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('materials', function (Blueprint $table) {
            $table->dropForeign('materials_category_id_foreign');
        });
		Schema::drop('materials');
	}
}
