<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateContactsTable.
 */
class CreateContactsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('phone', 50)->nullable();
			$table->string('email')->nullable();

			$table->char('group', 1)->nullable();
			$table->char('type', 1)->nullable();

			$table->char('entrepreneur',1)->nullable();
			$table->char('credility',1)->nullable();
			$table->char('networking', 1)->nullable();
			$table->char('empowerment',1)->nullable();
			$table->char('dreamer',1)->nullable();
			$table->char('register',1)->nullable();
			
			$table->text('observation')->nullable();
			$table->text('comments')->nullable();
			
			$table->dateTime('invitation')->nullable();
			$table->dateTime('plan')->nullable();
			$table->dateTime('fup')->nullable();
			$table->dateTime('closing')->nullable();
			$table->char('status',1)->default(0);
			
			$table->unsignedInteger('user_id')->nullable();
			$table->integer('import_id')->nullable();

			$table->foreign('user_id')->references('id')->on('users');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contacts', function (Blueprint $table) {
			$table->dropForeign('contacts_user_id_foreign');
		});

		Schema::drop('contacts');
	}
}
