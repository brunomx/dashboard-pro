<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUserVideosTable.
 */
class CreateUserVideosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_videos', function(Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('user_id')->nullable();
			$table->unsignedInteger('video_id')->nullable();
			$table->double('watch_time')->nullable();

			$table->char('completed', 1)->default(0);

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('video_id')->references('id')->on('videos');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_videos', function (Blueprint $table) {
    		$table->dropForeign('user_videos_user_id_foreign');
    		$table->dropForeign('user_videos_video_id_foreign');
		});
		Schema::drop('user_videos');
	}
}
