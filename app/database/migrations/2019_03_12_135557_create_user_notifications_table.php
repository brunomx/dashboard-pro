<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUserNotificationsTable.
 */
class CreateUserNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_notifications', function(Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('user_id')->nullable();
			$table->unsignedInteger('notification_id')->nullable();
			$table->char('status', 1)->default(0);

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('notification_id')->references('id')->on('notifications');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_notifications', function (Blueprint $table) {
    		$table->dropForeign('user_notifications_user_id_foreign');
    		$table->dropForeign('user_notifications_notification_id_foreign');
		});
		Schema::drop('user_notifications');
	}
}
