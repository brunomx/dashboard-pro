<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVideoTagsTable.
 */
class CreateVideoTagsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('video_tags', function(Blueprint $table) {
            $table->increments('id');
			
			$table->unsignedInteger('video_id');
			$table->unsignedInteger('tag_id');
			$table->integer('seconds')->nullable();
			$table->char('type', 1)->default(0);

			$table->foreign('video_id')->references('id')->on('videos');
			$table->foreign('tag_id')->references('id')->on('tags');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('video_tags', function (Blueprint $table) {
			$table->dropForeign('video_tags_video_id_foreign');
			$table->dropForeign('video_tags_tag_id_foreign');
		});

		Schema::drop('video_tags');
	}
}
