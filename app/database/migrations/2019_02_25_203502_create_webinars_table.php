<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateWebinarsTable.
 */
class CreateWebinarsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('webinars', function(Blueprint $table) {
			$table->increments('id');
   			$table->string('name');
   			$table->text('embed')->nullable();
   			$table->text('url')->nullable();
   			$table->dateTime('start_date')->useCurrent();
   			$table->dateTime('end_date')->useCurrent();
			
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('webinars');
	}
}
