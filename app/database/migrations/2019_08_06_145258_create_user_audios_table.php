<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUserAudiosTable.
 */
class CreateUserAudiosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_audios', function(Blueprint $table) {
            $table->increments('id');

			$table->unsignedInteger('user_id')->nullable();
			$table->unsignedInteger('audio_id')->nullable();
			$table->double('watch_time')->nullable();

			$table->char('completed', 1)->default(0);

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('audio_id')->references('id')->on('audio');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_audios', function (Blueprint $table) {
			$table->dropForeign('user_audios_user_id_foreign');
			$table->dropForeign('user_audios_audio_id_foreign');
		});

		Schema::drop('user_audios');
	}
}
