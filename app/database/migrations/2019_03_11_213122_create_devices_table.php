<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDevicesTable.
 */
class CreateDevicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devices', function(Blueprint $table) {
            $table->increments('id');
			$table->string('manufacturer')->nullable();
			$table->string('model')->nullable();
			$table->string('platform')->nullable();
			$table->string('serial')->nullable();
			$table->text('udid')->nullable();
			$table->string('version')->nullable();
			$table->text('token')->nullable();
			$table->string('player_ids')->nullable();
			$table->unsignedInteger('user_id')->nullable();

			$table->foreign('user_id')->references('id')->on('users');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('devices', function (Blueprint $table) {
			$table->dropForeign('devices_user_id_foreign');
		});

		Schema::drop('devices');
	}
}
