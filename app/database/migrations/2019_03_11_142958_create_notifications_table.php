<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateNotificationsTable.
 */
class CreateNotificationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
            $table->increments('id');
			$table->string('title');
			$table->string('subtitle')->nullable();
			$table->text('message');
			$table->text('link')->nullable();
			$table->char('system', 1)->nullable();
			$table->char('schedule', 1)->default(0);
			$table->dateTime('publication')->nullable();
			$table->integer('successful')->default(0);
			$table->integer('failed')->default(0);
			$table->integer('converted')->default(0);
			$table->integer('remaining')->default(0);
			$table->string('integration_id')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}
}
