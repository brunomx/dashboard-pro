<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateVideosTable.
 */
class CreateVideosTable extends Migration
{

	public function up()
	{
		Schema::create('videos', function(Blueprint $table) {
			$table->increments('id');
   			$table->string('name');
   			$table->text('code');
   			$table->text('url')->nullable();
   			$table->text('audio_url')->nullable();
   			$table->integer('sequence')->nullable();
   			$table->char('share', 1)->nullable();
   			$table->string('free', 1)->nullable();
   			$table->bigInteger('size')->nullable();
   			$table->dateTime('publication')->nullable();
			$table->string('status')->default('draft');
			
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('videos');
	}
}
