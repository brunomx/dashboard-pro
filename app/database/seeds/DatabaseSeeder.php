<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Entities\User;
use App\Entities\Course;
use App\Entities\TagCategory;
use App\Entities\Device;
use App\Entities\Package;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::create([
        //     'first_name' => 'Fernando',
        //     'last_name' => 'Mazzei',
        //     'username' => 'mazzei',
        //     'email' => 'mazzei@me.com',
        //     'permission' => 'app.admin',
        //     'password' => Hash::make('f3rn@nd0'),
        // ]);

        // User::create([
        //     'cpf' => '44841684050',
        //     'first_name' => 'Bruno',
        //     'last_name' => 'Mota',
        //     'email' => 'brunomx01@gmail.com',
        //     'password' => Hash::make('qazwsx'),
        // ]);

        // Course::create([
        //     'name' => 'Primeiros Passos',
        //     'type' => 1,
        // ]);

        // Package::create([
        //     'name' => 'PACK $100',
        //     'price' => 100,
        // ]);

        // Package::create([
        //     'name' => 'PACK $250',
        //     'price' => 250,
        // ]);

        // Package::create([
        //     'name' => 'PACK $500',
        //     'price' => 500,
        // ]);

        // Package::create([
        //     'name' => 'PACK $1000',
        //     'price' => 1000,
        // ]);

        // Package::create([
        //     'name' => 'PACK $3000',
        //     'price' => 3000,
        // ]);

        // Package::create([
        //     'name' => 'PACK $5000',
        //     'price' => 5000,
        // ]);

        // Package::create([
        //     'name' => 'PACK $10000',
        //     'price' => 10000,
        // ]);

        // Package::create([
        //     'name' => 'PACK $50000',
        //     'price' => 50000,
        // ]);


         User::create([
            'first_name' => 'Xpert',
            'last_name' => 'BackOffice',
            'username' => 'xpert_backoffice',
            'email' => 'xpert_backoffice@me.com',
            'permission' => 'app.api',
            'password' => Hash::make('xp3rtb4ck0ff1c3'),
        ]);

        // User::create([
        //     'first_name' => 'Presidente',
        //     'last_name' => 'Xpert',
        //     'username' => 'presidente_xpert',
        //     'package_id' => 8,
        //     'email' => 'presidente@xpert.com.py',
        //     'password' => Hash::make('Pr351d3nt3@xp3rt'),
        // ]);
    }
}
