@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Notificações
                    <a href="{{ route('notification.create') }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-company mb-0">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Título</th>
                                <th scope="col" class="border-0">Envio</th>
                                <th scope="col" class="border-0">Dispositivos</th>
                                <th scope="col" class="border-0">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($notifications as $key => $notification)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$notification->title}}</td>
                                <td>{{$notification->formatted_publication}}</td>
                                <td>{{$notification->users->count()}}
                                     {{-- <a href="#" data-toggle="modal" data-target="#listModal" data-id="{{$video->id}}" data-name="{{$video->name}}">{{$video->users->count()}}</a> --}}
                                </td>
                                <td>
                                    <a href="{{ route('notification.show', $notification->id) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">details</i>Detalhes
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$notification->id}}" data-name="{{$notification->title}}">
                                        <i class="material-icons mr-1">delete</i>Apagar
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'notification.remove'])
@include('templates.modal.list', ['route' => 'notification.list'])
@endsection