@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Detalhes da notificação
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($notification, ['route' => ['notification.update', $notification->id], 'method' => 'PUT']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4>Estatística de entrega</h4>
                            </div>
                            <div class="col-lg col-md-6 col-sm-6 mb-4">
                                <div class="stats-small stats-small--1 card card-small">
                                    <div class="card-body p-0 d-flex">
                                        <div class="d-flex flex-column m-auto">
                                            <div class="stats-small__data text-center">
                                                <span class="stats-small__label text-uppercase">Total de envios</span>
                                                <h6 class="stats-small__value count my-3">{{ $notification->total == null ? 0 : $notification->total }}</h6>
                                            </div>
                                        </div>
                                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-6 mb-4">
                                <div class="stats-small stats-small--1 card card-small">
                                    <div class="card-body p-0 d-flex">
                                        <div class="d-flex flex-column m-auto">
                                            <div class="stats-small__data text-center">
                                                <span class="stats-small__label text-uppercase">Pendente</span>
                                                <h6 class="stats-small__value count my-3">{{$notification->remaining}}</h6>
                                            </div>
                                            <div class="stats-small__data">
                                                <span class="stats-small__percentage stats-small__percentage--increase">{{ $notification->remaining > 0 ? number_format(($notification->remaining / $notification->total) * 100, 2, ',', '.') : $notification->remaining }}%</span>
                                            </div>
                                        </div>
                                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-6 mb-4">
                                <div class="stats-small stats-small--1 card card-small">
                                    <div class="card-body p-0 d-flex">
                                        <div class="d-flex flex-column m-auto">
                                            <div class="stats-small__data text-center">
                                                <span class="stats-small__label text-uppercase">Entregues</span>
                                                <h6 class="stats-small__value count my-3">{{$notification->successful}}</h6>
                                            </div>
                                            <div class="stats-small__data">
                                                <span class="stats-small__percentage stats-small__percentage--increase">{{ $notification->successful > 0 ? number_format(($notification->successful / $notification->total) * 100, 2, ',', '.') : $notification->remaining }}%</span>
                                            </div>
                                        </div>
                                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-6 mb-4">
                                <div class="stats-small stats-small--1 card card-small">
                                    <div class="card-body p-0 d-flex">
                                        <div class="d-flex flex-column m-auto">
                                            <div class="stats-small__data text-center">
                                                <span class="stats-small__label text-uppercase">Interagiram</span>
                                                <h6 class="stats-small__value count my-3">{{$notification->converted}}</h6>
                                            </div>
                                            <div class="stats-small__data">
                                                <span class="stats-small__percentage stats-small__percentage--increase">{{ $notification->converted > 0 ? number_format(($notification->converted / $notification->total) * 100, 2, ',', '.') : $notification->converted }}%</span>
                                            </div>
                                        </div>
                                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg col-md-6 col-sm-6 mb-4">
                                <div class="stats-small stats-small--1 card card-small">
                                    <div class="card-body p-0 d-flex">
                                        <div class="d-flex flex-column m-auto">
                                            <div class="stats-small__data text-center">
                                                <span class="stats-small__label text-uppercase">Falha <br/>(sem permissão)</span>
                                                <h6 class="stats-small__value count my-3">{{$notification->failed}}</h6>
                                            </div>
                                            <div class="stats-small__data">
                                                <span class="stats-small__percentage stats-small__percentage--increase">{{ $notification->failed > 0 ? number_format(($notification->failed / $notification->total) * 100, 2, ',', '.') : $notification->failed }}%</span>
                                            </div>
                                        </div>
                                        <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('title', 'Título') !!}
                                {!! Form::text('title', $notification->title, ['class' => 'form-control', 'placeholder' => 'Título', 'disabled' => true]) !!}
                            </div>
                            
                            <div class="col-md-6">
                                {!! Form::label('subtitle', 'Subtítulo') !!}
                                {!! Form::text('subtitle', $notification->subtitle, ['class' => 'form-control', 'placeholder' => 'Subtítulo', 'disabled' => true]) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                {!! Form::label('message', 'Mensagem') !!}
                                {!! Form::textarea('message', $notification->message, ['class' => 'form-control', 'placeholder' => 'Mensagem', 'disabled' => true]) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('link', 'Link') !!}
                                {!! Form::text('link', $notification->link, ['class' => 'form-control', 'placeholder' => 'Link', 'disabled' => true]) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-6">
                                <h5>Sistema Operacional</h5>
                                <div class="row">
                                    <div class="col link-box-upload">
                                        {!! Form::radio('system', $notification->system, true, ['id' => 'type-'.$notification->system, 'class' => 'radio-files', 'disabled' => true]) !!}
                                        {!! Form::label('type-'.$notification->system, $notification->systemName[$notification->system]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="name">Data e horário de envio</label>
                                {!! Form::datetimeLocal('publication', \Carbon\Carbon::parse($notification->publication), ['class' => 'form-control', 'disabled' => true]) !!}
                            </div>
                        </div>
                      <div class="row py-2">
                            <div class="col-6">
                                <div>Usuários que receberam ou interagiram com a notificação <strong>({{$notification->users->count()}})</strong>:</div>
                                <ul>
                                    @foreach($notification->users as $key => $user)
                                        <li>{{$user->name}}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $notification->package == null ? null : $notification->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes', 'disabled' => true]) !!}
                            </div>
                        </div>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection