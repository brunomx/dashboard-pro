@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Notificação
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
            {!! Form::open(['route' => 'notification.store', 'method' => 'post']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('title', 'Título') !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título']) !!}
                            </div>
  
                            <div class="col-md-6">
                                {!! Form::label('subtitle', 'Subtítulo') !!}
                                {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => 'Subtítulo']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                {!! Form::label('message', 'Mensagem') !!}
                                {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Mensagem']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('link', 'Link') !!}
                                {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'Link']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="name">Data e horário de envio</label>
                                {!! Form::datetimeLocal('publication', \Carbon\Carbon::now()->addMinute(5), ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6">
                                <h5>Filtrar por Sistema Operacional</h5>
                                <div class="row">
                                    @foreach($notification->systemName as $key => $type)
                                    <div class="col link-box-upload">
                                        {!! Form::radio('system', $key, $key == 0 ? true : false, ['id' => 'type-'.$key, 'class' => 'radio-files']) !!}
                                        {!! Form::label('type-'.$key, $type) !!}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                {!! Form::label('users', 'Usuários (Caso não selecione nenhum usuário será enviado para todos)') !!} 
                                {!! Form::select('users[]', $users, null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('package_id', 'Restringir conteúdo ao pacote') !!} 
                                {!! Form::select('package_id', $packages, null, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection