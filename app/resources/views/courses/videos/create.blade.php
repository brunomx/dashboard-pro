@extends('templates.master')

@section('content-view')
<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Cadastrar Vídeo
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::open(['route' => ['course.video.store', $course_id], 'method' => 'post', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Name</label>
                            </div>
                            <div class="col-md-6">
                                <label for="name">Url para download</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Url']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="name">Embed do vídeo</label>
                            </div>
                            <div class="col-md-6">
                                <label for="name">Versão em áudio (Formato MP3)</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::textarea('embed', null, ['class' => 'form-control', 'placeholder' => 'Embed']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::file('audio', ['class' => 'form-control', 'id' => 'audio-upload', 'style'=>'display:none']) !!}
                                <audio controls class="preview-audio"></audio>
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-audio']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-audio']) !!}  
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-6">
                                <label for="package_id">Liberar ao completar o vídeo</label>
                                {!! Form::select('package_id', $before_list, null, ['class' => 'form-control', 'placeholder' => 'Sem vídeo anterior']) !!}
                            </div>
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, null, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                <label for="name">Data e hora do envio de notificação</label>
                                {!! Form::datetimeLocal('publication', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-3 offset-md-9">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
    @include('templates.modal.upload-error')
@endsection

@section('js-view')
    @include('templates.js.upload-audio')
@endsection
