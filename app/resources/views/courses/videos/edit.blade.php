@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Vídeo
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($video, ['route' => ['course.video.update', $course_id, $video->id], 'method' => 'PUT', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="row">
                            <div class="col-6">
                                <label for="name">Nome</label>
                            </div>
                            <div class="col-6">
                                <label for="name">Url para download</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::text('name', $video->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::text('url', $video->url, ['class' => 'form-control', 'placeholder' => 'Url']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                <label for="name">Embed do vídeo</label>
                            </div>
                            <div class="col-6">
                                <label for="name">Versão em áudio (Formato MP3)</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                {!! Form::textarea('embed', $video->embed, ['class' => 'form-control', 'placeholder' => 'Embed']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::file('audio', ['class' => 'form-control', 'id' => 'audio-upload', 'style'=>'display:none']) !!}
                                
                                @if($video->audio)
                                    <audio controls class="preview-audio" style="display:block">
                                        <source src="{{ Storage::url($video->audio) }}" type="audio/mpeg">
                                    </audio>
                                @else
                                    <audio controls class="preview-audio"></audio>
                                @endif
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-audio']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-audio']) !!}  
                                <input type="hidden" name="remove-audio" class="input-hidden" id="remove-hidden-audio" value="0">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-6">
                                <label for="before_id">Liberar ao completar o vídeo</label>
                                {!! Form::select('before_id', $before_list, $video->before == null ? null : $video->before->id, ['class' => 'form-control', 'placeholder' => 'Sem vídeo anterior']) !!}
                            </div>
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $video->package == null ? null : $video->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                <label for="name">Data e hora do envio de notificação</label>
                                {!! Form::datetimeLocal('publication', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-4 offset-8">
                                {!! Form::submit('Savar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
    @include('templates.modal.upload-error')
@endsection

@section('js-view')
    @include('templates.js.upload-audio')
@endsection
