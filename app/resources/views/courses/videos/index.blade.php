@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Vídeos
                    <a href="{{ route('course.video.create', $course_id) }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                    @if(request()->is('course/'.env('FIRST_STEPS').'/video*') === FALSE && request()->is('course/'.env('DAILY_VIDEOS').'/video*') === FALSE)
                        <a href="{{ route('course.index') }}" class="text-right">
                            <button type="button" class="btn btn-primary btn-sm btn-add btn-back">
                                <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                            </button>
                        </a> 
                    @endif
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-course mb-0" id="sortFixed" data-course="{{$course_id}}">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Miniatura</th>
                                <th scope="col" class="border-0">Nome</th>
                                <th scope="col" class="border-0">Pacote</th>
                                <th scope="col" class="border-0">Ações</th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($videos as $key => $video)
                            <tr data-id="{{$video->id}}">
                                <td>{{$key + 1}}</td>
                                <td><img src="{{$video->thumbnail}}" class="thumb-list"/></td>
                                <td>{{$video->name}}</td>
                                <td>{{$video->package == null ? '-' : $video->package->name}}</td>
                                <td>
                                    <button type="button" class="btn btn-success btn-sm text-center" data-toggle="modal" data-target="#listModal" data-id="{{$video->id}}" data-name="{{$video->name}}">
                                        <i class="material-icons mr-1">play_arrow</i> Views
                                    </button>
                                    <a href="{{ route('course.video.edit', ['course_id' => $course_id, 'id' => $video->id]) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">edit</i>Editar
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$video->id}}" data-name="{{$video->name}}" data-entity="{{$course_id}}">
                                        <i class="material-icons mr-1">delete</i>Deletar
                                    </button>
                                </td>
                                <td><i class="material-icons drag-item">drag_handle</i></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'course.video.remove'])
@include('templates.modal.list', ['route' => 'course-video-details'])
@endsection

@section('js-view')
    @include('templates.js.sortable', ['url' => 'course-video-sequence'])
@endsection

@section('css-view')
    <link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
@endsection