@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Cursos
                    <a href="{{ route('course.create') }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-course mb-0" id="sortFixed">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Imagem</th>
                                <th scope="col" class="border-0">Nome</th>
                                <th scope="col" class="border-0">Vídeos</th>
                                <th scope="col" class="border-0">Pacotes</th>
                                <th scope="col" class="border-0">Ações</th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($courses as $key => $course)
                            <tr data-id="{{$course->id}}">
                                <td>{{$key + 1}}</td>
                                <td>
                                    @if($course->image)
                                    <img src={{ Storage::url($course->image) }} class="py-2" style="max-width:150px">
                                    @endif
                                </td>
                                <td>{{$course->name}}</td>
                                <td>{{$course->videos->count()}}</td>
                                <td>{{$course->package == null ? '' : $course->package->name}}</td>
                                <td>
                                    <a href="{{ route('course.video.index', $course->id) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">settings</i>Vídeos
                                        </button>
                                    </a>
                                    <a href="{{ route('course.edit', $course->id) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">edit</i>Editar
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$course->id}}" data-name="{{$course->name}}">
                                        <i class="material-icons mr-1">delete</i>Deletar
                                    </button>
                                </td>
                                <td><i class="material-icons drag-item">drag_handle</i></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'course.remove'])
@endsection

@section('js-view')
  @include('templates.js.sortable', ['url' => 'course-sequence'])
@endsection

@section('css-view')
<link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
@endsection