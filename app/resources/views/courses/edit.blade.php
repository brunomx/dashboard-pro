@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Curso
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($course, ['route' => ['course.update', $course->id], 'method' => 'PUT', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', $course->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('image', 'Imagem') !!}
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                @if($course->image)
                                    <img src={{ Storage::url($course->image) }} class="preview-image"/>
                                @else
                                    <img class="preview-image"/>
                                @endif
                                
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}  
                                <input type="hidden" name="remove-image" class="input-hidden" id="remove-hidden-image" value="0">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $course->package == null ? null : $course->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection

@section('js-view')
    @include('templates.js.upload-image')
@endsection
