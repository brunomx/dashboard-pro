@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Reuniões
                    <a href="{{ route('meeting.create') }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-course mb-0" id="sortFixed">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Imagem</th>
                                <th scope="col" class="border-0">Nome</th>
                                <th scope="col" class="border-0">Data</th>
                                <th scope="col" class="border-0">Pacote</th>
                                <th scope="col" class="border-0">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($meetings as $key => $meeting)
                            <tr data-id="{{$meeting->id}}">
                                <td>{{$key + 1}}</td>
                                <td>
                                    @if($meeting->image)
                                    <img src={{ Storage::url($meeting->image) }} class="py-2" style="max-width:150px">
                                    @endif
                                </td>
                                <td>{{$meeting->name}}</td>
                                <td>{{$meeting->formatted_date}}</td>
                                <td>{{$meeting->package == null ? '' : $meeting->package->name}}</td>
                                <td>
                                    <a href="{{ route('meeting.edit', $meeting->id) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">edit</i>Editar
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$meeting->id}}" data-name="{{$meeting->name}}">
                                        <i class="material-icons mr-1">delete</i>Deletar
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'meeting.remove'])
@endsection