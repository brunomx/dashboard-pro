@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Curso
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($meeting, ['route' => ['meeting.update', $meeting->id], 'method' => 'PUT', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', $meeting->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('description', 'Descrição') !!}
                                {!! Form::textarea('description', $meeting->description, ['class' => 'form-control', 'placeholder' => 'Descrição']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                {!! Form::label('date', 'Data') !!}
                                {!! Form::datetimeLocal('date',  \Carbon\Carbon::parse($meeting->date), ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $meeting->package == null ? null : $meeting->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                {!! Form::label('address_address', 'Endereço') !!}
                                {!! Form::text('address_address', $meeting->address_address, ['class' => 'form-control map-input', 'placeholder' => 'Endereço', 'id' => 'address-input']) !!}
                                <input type="hidden" name="address_latitude" id="address-latitude" value="{{ $meeting->address_latitude }}" />
                                <input type="hidden" name="address_longitude" id="address-longitude" value="{{ $meeting->address_longitude }}" />
                                <div id="address-map-container" style="width:100%;height:400px;" class="py-3">
                                    <div style="width: 100%; height: 100%" id="address-map"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                {!! Form::label('image', 'Imagem') !!}
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                @if($meeting->image)
                                    <img src={{ Storage::url($meeting->image) }} class="preview-image"/>
                                @else
                                    <img class="preview-image"/>
                                @endif
                                
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}  
                                <input type="hidden" name="remove-image" class="input-hidden" id="remove-hidden-image" value="0">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection
@section('js-view')
    @include('templates.js.upload-image')
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
    @include('templates.js.map')
@endsection
