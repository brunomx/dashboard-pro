@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Webinário
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::open(['route' => 'webinar.store', 'method' => 'post']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Nome</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-6">
                                <label for="url">Url</label>
                                {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'Url']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="embed">Embed</label>
                                {!! Form::textarea('embed', null, ['class' => 'form-control', 'placeholder' => 'Embed']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                <label for="start_date">Data de início</label>
                                {!! Form::datetimeLocal('start_date', \Carbon\Carbon::now(), ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-6">
                                <label for="end_date">Data de Término</label>
                                {!! Form::datetimeLocal('end_date', \Carbon\Carbon::now()->addDays(2), ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-3 offset-md-9">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection