@if(session('success'))
    <div class="alert {{session('success')['success'] == true ? 'alert-success' : 'alert-danger'}} alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>{{ session('success')['messages'] }} </strong>
    </div>
@endif