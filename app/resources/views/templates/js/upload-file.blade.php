<script>
    $('#select-pdf').on('click', function(){
        $('#pdf-upload').click();
    });

    $('#remove-pdf').on('click', function(){
        $('#pdf-upload').val('');
        $('#remove-hidden-pdf').val(1);
        $('.preview-pdf').hide();
    });

    $('#form-multi-file .radio-files').change(function(e){
        var target = $(e.currentTarget).val();
        $('.box-upload').removeClass('active').eq(target).addClass('active');
    });

    $('#select-slide').on('click', function(){
        $('#slide-upload').click();
    });

    $('#form-multi-file').on('click', '#remove-slide', function(){
        $('#slide-upload').val('');
        $('#remove-hidden-slide').val(1);
        $('.preview-slide').hide();
    });

    document.getElementById("pdf-upload").onchange = function () {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            var extension = e.target.result.split(';').shift();
            if(extension != 'data:application/pdf'){
                $('#audio-upload').val('');
                $('#modalUploadError').modal();
                $('#pdf-upload').val('');
            } else {
                document.getElementsByClassName("preview-pdf")[0].src = e.target.result;
                $('#remove-hidden-pdf').val(1);
                $('.preview-pdf').show();
            }
        };        
        reader.readAsDataURL(this.files[0]);
    };

    document.getElementById("slide-upload").onchange = function () {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            var extension = e.target.result.split(';').shift();
            if(extension != 'data:application/vnd.openxmlformats-officedocument.presentationml.presentation'){
                $('#audio-upload').val('');
                $('#modalUploadError').modal();
                $('.preview-audio').removeAttr('src').hide();
            } else {
                document.getElementsByClassName("preview-slide")[0].href = e.target.result;
                $('#remove-hidden-slide').val(1);
                $('.preview-slide').show();
            }
        };        
        reader.readAsDataURL(this.files[0]);
    };

</script>