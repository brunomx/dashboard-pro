  <script src="https://code.jquery.com/ui/jquery-ui-git.js"></script>
  <script>
    $('td, th', '#sortFixed').each(function () {
        var cell = $(this);
        cell.width(cell.width());
    });
    $("table tbody").sortable( {
        update: function( event, ui ) {
            var itens = [];
            $(this).children().each(function(index) {
                itens.push($(this).data('id'))
                
                $(this).find('td').first().html(index + 1)
            });
            var data = { itens : JSON.stringify(itens)};

            if($("#sortFixed").data('course') != undefined){
                data['course_id'] = $("#sortFixed").data('course');
            }

            updatePosition(data);
        }
    }).disableSelection();
    function updatePosition(data){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '/{{$url}}',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            success: function(data){
            },
            error: function(data){
            }
        })
    }
</script>