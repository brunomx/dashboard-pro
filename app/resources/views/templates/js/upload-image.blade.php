<script>
    $('#select-image').on('click', function(){
        $('#image-upload').click();
    });

    $('#remove-image').on('click', function(){
        $('.preview-image').removeAttr('src');
        $('#image-upload').val('');
        $('#remove-hidden-image').val(1);
    });
    
    document.getElementById("image-upload").onchange = function () {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            document.getElementsByClassName("preview-image")[0].src = e.target.result;
            $('#remove-hidden-image').val(1);
        };        
        reader.readAsDataURL(this.files[0]);
    };
</script>