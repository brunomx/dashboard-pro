<script>
    $('#select-audio').on('click', function(){
        $('#audio-upload').click();
    });

    $('#remove-audio').on('click', function(){
        $('.preview-audio').removeAttr('src').hide();;
        $('#audio-upload').val('');
        $('#remove-hidden-audio').val(1);
    });

    $('.remove-time-tag').on('click', function(e){
        var id = $(e.currentTarget).attr('id');
        $('#item-' + id).remove();
    });

    $('#add-time-tag').on('click', function(e){
        document.getElementById('time-tags-group').appendChild(document.getElementById('time-tags-template').content.cloneNode(true));
    });
    
    document.getElementById("audio-upload").onchange = function () {
        var reader = new FileReader();
        var suported_extensions = ['data:application/vnd.openxmlformats-officedocument.presentationml.presentation'];
        
        reader.onload = function (e) {
            var extension = e.target.result.split(';').shift();
            if(extension != 'data:audio/mp3'){
                $('#audio-upload').val('');
                $('#modalUploadError').modal();
                $('.preview-audio').removeAttr('src').hide();
            } else {
                document.getElementsByClassName("preview-audio")[0].src = e.target.result;
                $('.preview-audio').show();
            }
            $('#remove-hidden-audio').val(1);
        };
        reader.readAsDataURL(this.files[0]);
    };
</script>