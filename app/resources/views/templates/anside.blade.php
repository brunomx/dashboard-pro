<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="{{ route('dashboard') }}" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 80px;" src="{{ asset('images/logo-xpert.png') }}">
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
        <div class="input-group input-group-seamless ml-3">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
            <input class="navbar-search form-control" type="text" placeholder="Procurar..." aria-label="Search"> 
        </div>
    </form>
    <div class="nav-wrapper">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link {{ request()->is('course/'.env('DAILY_VIDEOS').'/video*') ? 'active' : '' }}" href="{{ route('course.video.index', 2) }}">
                    <i class="material-icons">date_range</i>
                    <span>Dose Diária</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('audio*') ? 'active' : '' }}" href="{{ route('audio.index') }}">
                    <i class="material-icons">audiotrack</i>
                    <span>Áudios Diários</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('course/'.env('FIRST_STEPS').'/video*') ? 'active' : '' }}" href="{{ route('course.video.index', 1) }}">
                    <i class="material-icons">video_label</i>
                    <span>Primeiros passos</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('course*') && request()->is('course/'.env('FIRST_STEPS').'/video*') == FALSE && request()->is('course/'.env('DAILY_VIDEOS').'/video*') == FALSE ? 'active' : '' }}" href="{{ route('course.index') }}">
                    <i class="material-icons">local_library</i>
                    <span>Cursos</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('material*') && request()->is('material-category*') === FALSE ? 'active' : '' }}" href="{{ route('material.index') }}">
                    <i class="material-icons">description</i>
                    <span>Materiais</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('material-category*') ? 'active' : '' }}" href="{{ route('material-category.index') }}">
                    <i class="material-icons">label</i>
                    <span>Categ. de Materiais</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('webinar*') ? 'active' : '' }}" href="{{ route('webinar.index') }}">
                    <i class="material-icons">video_call</i>
                    <span>Webinários</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('notification*') ? 'active' : '' }}" href="{{ route('notification.index') }}">
                    <i class="material-icons">notifications</i>
                    <span>Notificações</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('meeting*') ? 'active' : '' }}" href="{{ route('meeting.index') }}">
                    <i class="material-icons">event</i>
                    <span>Reuniões</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('qualification*') ? 'active' : '' }}" href="{{ route('qualification.index') }}">
                    <i class="material-icons">grade</i>
                    <span>Leadership</span>
                </a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link {{ request()->is('tag*') && request()->is('tag-category*') === FALSE ? 'active' : '' }}" href="{{ route('tag.index') }}">
                    <i class="material-icons">label</i>
                    <span>Tags</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ request()->is('tag-category*') ? 'active' : '' }}" href="{{ route('tag-category.index') }}">
                    <i class="material-icons">label</i>
                    <span>Categ. de Tags</span>
                </a>
            </li> --}}
            <li class="nav-item">
                <a class="nav-link {{ request()->is('package*') ? 'active' : '' }}" href="{{ route('package.index') }}">
                    <i class="material-icons">add_alert</i>
                    <span>Renovação de Pacote</span>
                </a>
            </li>
        </ul>
    </div>
</aside>