<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteModalLabel">{{$title}}?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong></strong>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        {!! Form::open(['route' => $route, 'method' => 'POST']) !!}
					<input type="hidden" name="id" class="input-hidden">
					<input type="hidden" name="entity_id" class="input-hidden">
					{!! Form::submit('Confirmar',['class' => 'btn btn-danger']) !!}
				{!! Form::close() !!}
      </div>
    </div>
  </div>
</div>