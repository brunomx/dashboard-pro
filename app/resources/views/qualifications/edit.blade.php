@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Leadership
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($qualification, ['route' => ['qualification.update', $qualification->id], 'method' => 'PUT', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', $qualification->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('level', 'Qualificação') !!}
                                {!! Form::select('level', $qualification->options, $qualification->level, ['class' => 'form-control select-qualification',]) !!}
                            </div>
                        </div>
                        <div class="row py-2" id="addressOptions">
                            <div class="col-md-6">
                                {!! Form::label('city', 'Cidade') !!}
                                {!! Form::text('city', $qualification->city, ['class' => 'form-control', 'placeholder' => 'Cidade']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('state', 'Estado') !!}
                                {!! Form::text('state', $qualification->state, ['class' => 'form-control', 'placeholder' => 'Estado']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                {!! Form::label('month', 'Mês') !!}
                                {!! Form::select('month', $qualification->months, $qualification->month, ['class' => 'form-control',]) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('year', 'Ano') !!}
                                {!! Form::text('year', $qualification->year, ['class' => 'form-control', 'placeholder' => 'Ano']) !!}
                            </div>
                        </div>
                        <div class="row py-2" id="imageOptions" style="{{$qualification->level < 3 ? 'display:none' : ''}}">
                            <div class="col-6">
                                {!! Form::label('image', 'Imagem') !!}
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                @if($qualification->image)
                                    <img src={{ Storage::url($qualification->image) }} class="preview-image"/>
                                @else
                                    <img class="preview-image"/>
                                @endif
                                
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}  
                                <input type="hidden" name="remove-image" class="input-hidden" id="remove-hidden-image" value="0">
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                        
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection

@section('js-view')
    @include('templates.js.upload-image')
    @include('templates.js.select-qualification')
@endsection
