@extends('templates.master')
@section('content-view')

<!-- Page Header -->
<div class="page-header row no-gutters py-4">
</div>
<!-- End Small Stats Blocks -->
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar material
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($material, ['route' => ['material.update', $material->id], 'method' => 'PUT', 'files' => true, 'id' => 'form-multi-file']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', $material->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('category_id', 'Categoria') !!}
                                {!! Form::select('category_id', $categories, $material->category == null ? null : $material->category->id, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-6">
                                <h5>Tipo</h5>
                                <div class="row">
                                    @foreach($material->typeName as $key => $type)
                                    <div class="col link-box-upload">
                                        {!! Form::radio('type', $key, $key == $material->type ? true : false, ['id' => 'type-'.$key, 'class' => 'radio-files', 'disabled' => true]) !!}
                                        {!! Form::label('type-'.$key, $type) !!}
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $material->package == null ? null : $material->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 box-upload {{ $material->type == 0 ? 'active' : ''}}">
                                {!! Form::file('pdf', ['class' => 'form-control', 'id' => 'pdf-upload', 'style'=>'display:none']) !!}

                                @if($material->path && $material->type == 0)
                                    <iframe src="{{ Storage::url($material->path) }}" class="preview-pdf show" style="width:100%; height:300px;" frameborder="0"></iframe>
                                @else
                                    <iframe src="" class="preview-pdf" style="width:100%; height:300px;" frameborder="0"></iframe>
                                @endif

                                <input type="hidden" name="remove-pdf" class="input-hidden" id="remove-hidden-pdf" value="0">
                                {!! Form::button('Selecione o PDF', ['class' => 'btn btn-accent', 'id' => 'select-pdf']) !!}  
                                {!! Form::button('Remover Arquivo', ['class' => 'btn btn-danger', 'id' => 'remove-pdf']) !!}  
                            </div>
                            <div class="col-md-6 box-upload {{ $material->type == 1 ? 'active' : ''}}">
                                {!! Form::file('audio', ['class' => 'form-control', 'id' => 'audio-upload', 'style'=>'display:none']) !!}

                                @if($material->path && $material->type == 1)
                                    <audio controls class="preview-audio" style="display:block">
                                        <source src="{{ Storage::url($material->path) }}" type="audio/mpeg">
                                    </audio>
                                @else
                                    <audio controls class="preview-audio"></audio>
                                @endif

                                <input type="hidden" name="remove-audio" class="input-hidden" id="remove-hidden-audio" value="0">
                                {!! Form::button('Selecione o Áudio', ['class' => 'btn btn-accent', 'id' => 'select-audio']) !!}  
                                {!! Form::button('Remover Arquivo', ['class' => 'btn btn-danger', 'id' => 'remove-audio']) !!}  
                            </div>
                            <div class="col-md-12 box-upload {{ $material->type == 2 ? 'active' : ''}}">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::label('embed', 'Embed do vídeo') !!}
                                        {!! Form::textarea('embed', $material->embed, ['class' => 'form-control', 'placeholder' => 'Embed']) !!}
                                    </div>
                                    <div class="col-6">
                                        {!! Form::label('url', 'Link para download') !!}
                                        {!! Form::text('url', $material->url, ['class' => 'form-control', 'placeholder' => 'Link']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 box-upload {{ $material->type == 3 ? 'active' : ''}}">
                                {!! Form::file('slide', ['class' => 'form-control', 'id' => 'slide-upload', 'style' => 'display:none']) !!}

                                @if($material->path && $material->type == 3)
                                    <a href="{{ Storage::url($material->path) }}" target="_blank" class="preview-slide" style="display:block">
                                        <button type="button" class="btn btn-success btn-sm text-center btn-block">
                                            <i class="material-icons mr-1">slideshow</i>Visualizar Arquivo
                                        </button>
                                    </a>
                                @else
                                    <a href="" target="_blank" class="preview-slide">
                                        <button type="button" class="btn btn-success btn-sm text-center btn-block">
                                            <i class="material-icons mr-1">slideshow</i>Visualizar Arquivo
                                        </button>
                                    </a>
                                @endif

                                <br/><br/>
                                <input type="hidden" name="remove-slide" class="input-hidden" id="remove-hidden-slide" value="0">
                                {!! Form::button('Selecione o Slide', ['class' => 'btn btn-accent', 'id' => 'select-slide']) !!}  
                                {!! Form::button('Remover Arquivo', ['class' => 'btn btn-danger', 'id' => 'remove-slide']) !!}  
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.upload-error')
@endsection
@section('js-view')
    @include('templates.js.upload-file')
    @include('templates.js.upload-audio')
@endsection