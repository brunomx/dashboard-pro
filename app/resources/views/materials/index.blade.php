@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Materiais
                    <a href="{{ route('material.create') }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-company mb-0" id="sortFixed">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Nome</th>
                                <th scope="col" class="border-0">Tipo</th>
                                <th scope="col" class="border-0">Categoria</th>
                                <th scope="col" class="border-0">Pacote</th>
                                <th scope="col" class="border-0">Acões</th>
                                <th scope="col" class="border-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($materials as $key => $material)
                            <tr data-id="{{$material->id}}">
                                <td>{{$key + 1}}</td>
                                <td>{{$material->name}}</td>
                                <td>{{$material->type_name}}</td>
                                <td>{{ is_null($material->category) ? '' : $material->category->name }}</td>
                                <td>{{$material->package == null ? '' : $material->package->name}}</td>
                                <td>
                                    <a href="{{ route('material.edit', $material->id) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">edit</i>Editar
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$material->id}}" data-name="{{$material->name}}">
                                        <i class="material-icons mr-1">delete</i>Deletar
                                    </button>
                                </td>
                                <td><i class="material-icons drag-item">drag_handle</i></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'material.remove'])
@endsection

@section('js-view')
    @include('templates.js.sortable', ['url' => 'material-sequence'])
@endsection

@section('css-view')
    <link rel="stylesheet" href="https://code.jquery.com/ui/jquery-ui-git.css">
@endsection