@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Link
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
            {!! Form::open(['route' => ['program.link.store', $program_id], 'method' => 'post', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="row">
                            <div class="col-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('code', 'Código') !!}
                                {!! Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'Código']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                <label for="name">Imagem</label>
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                <img class="preview-image" />
                                {!! Form::button('Select', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}
                                {!! Form::button('Remove', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}
                            </div>
                            <div class="col-6">
                                {!! Form::label('link', 'Link') !!}
                                {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'Link']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-4 offset-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
    @include('templates.modal.upload-error')
@endsection

@section('js-view')
    @include('templates.js.upload-image')
@endsection