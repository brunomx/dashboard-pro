@extends('templates.master')
@section('content-view')

<!-- End Small Stats Blocks -->
<br>
@include('templates.alert')
<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Lista de Links
                    <a href="{{ route('program.link.create', $program_id) }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">add_circle</i> Adicionar
                        </button>
                    </a>
                    <a href="{{ route('program.index') }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add btn-back">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a> 
                </h6>
            </div>
            <div class="card-body p-0">
                <div class="card-body p-0 pb-3 text-center">
                    <table class="table table-company mb-0">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Imagem</th>
                                <th scope="col" class="border-0">Nome</th>
                                <th scope="col" class="border-0">Código</th>
                                <th scope="col" class="border-0">Acões</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($links as $key => $link)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>
                                    @if($link->image)
                                        <img src={{ Storage::url($link->image) }} class="py-2" style="max-width:150px">
                                    @endif
                                </td>
                                <td>{{ substr($link->name, 0 , 30) . '...' }}</td>
                                <td>{{ $link->code }}</td>
                                <td>
                                    <a href="{{ route('program.link.edit', ['program_id' => $program_id, 'id' => $link->id]) }}">
                                        <button type="button" class="btn btn-primary btn-sm text-center">
                                            <i class="material-icons mr-1">edit</i>Editar
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-sm text-center" data-toggle="modal" data-target="#deleteModal" data-id="{{$link->id}}" data-name="{{$link->name}}" data-entity="{{$program_id}}">
                                        <i class="material-icons mr-1">delete</i>Deletar
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.delete', ['title' => 'Deseja excluir', 'route' => 'program.link.remove'])
@endsection