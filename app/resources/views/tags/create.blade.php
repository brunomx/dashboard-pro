@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Tag
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
            {!! Form::open(['route' => 'tag.store', 'method' => 'post']) !!}
                <div class="form-row">
                    <div class="form-group col-12">
                        <div class="row">
                            <div class="col-6">
                                {{ Form::label('name', 'Nome') }}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            <div class="col-6">
                                {{ Form::label('category_id', 'Categoria') }}
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                {{ Form::label('description', 'Descricão') }}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Descricão']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection