@extends('templates.master')
@section('content-view')

<!-- Page Header -->
<div class="page-header row no-gutters py-4">
</div>
<br>
@include('templates.alert')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- End Small Stats Blocks -->
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Create user 
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Back
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::open(['route' => 'user.change.password', 'method' => 'post', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('current', 'Senha atual') !!}
                                {!! Form::password('current', ['class' => 'form-control', 'placeholder' => 'Senha atual']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('new_password', 'Nova senha') !!}
                                {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => 'Nova senha']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                {!! Form::label('confirm_password', 'Confirmar senha') !!}
                                {!! Form::password('confirm_password', ['class' => 'form-control', 'placeholder' => 'Confirmar senha']) !!}
                            </div>
                            <div class="col-md-6">
                                <br>
                                {!! Form::submit('Change', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.upload-error')
@endsection
@section('js-view')
    @include('templates.js.upload-image')
@endsection