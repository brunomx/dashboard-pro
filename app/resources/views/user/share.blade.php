@extends('templates.empty')
@section('content-view')
    <div class="card card-small">
        <div class="card-header border-bottom">
            <h6 class="m-0 text-center"><img id="main-logo" style="max-width: 150px;" src="{{ asset('images/logo-xpert.png') }}"></h6>
        </div>
        <div class="card-body flex-column">
            <iframe src="https://player.vimeo.com/video/340460579" width="100%" height="400" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

            <div class="row">
                <div class="col-8 offset-2 py-3">
                    <a href="https://myxpert.me/register/{{$username}}" class="text-right">
                        <button type="button" class="btn btn-danger btn-lg btn-block">Cadastre-se agora</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection