@extends('templates.master')
@section('content-view')

<!-- Page Header -->
<!-- End Small Stats Blocks -->
<br/>
<div class="row">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Perfil
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i> Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
            {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PUT']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('first_name', 'First Name') !!}
                                {!! Form::text('first_name', $user->first_name, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('last_name', 'Last Name') !!}
                                {!! Form::text('last_name', $user->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                            </div>
                        </div>

                        <div class="row py-3">
                            <div class="col-md-6 offset-6">
                                <br>
                                {!! Form::submit('Save', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@endsection