@extends('templates.blank')
@section('content-view')
    <div class="card card-small">
        <div class="card-header border-bottom">
            <h6 class="m-0 text-center"><img id="main-logo" style="max-width: 150px;" src="{{ asset('images/logo-xpert.png') }}"></h6>
        </div>
        <div class="card-body flex-column">
        {!! Form::open(['route' => 'user.login', 'method' => 'POST']) !!}
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="email">E-mail</label>
                     {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                </div>
                <div class="form-group col-md-12">
                    <label for="password">Password</label>
                    {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
                </div>
                <div class="form-group col-md-12 text-right">
                     {!! Form::submit('Login', ['class' => 'btn btn-accent']) !!}                  
                </div>
            </div>
        {!! Form::close() !!}
        </div>
    </div>
    <br>
     @include('templates.alert')
@endsection