@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Notificação
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
            {!! Form::open(['route' => 'package.store', 'method' => 'post']) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('title', 'Título') !!}
                                {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Título']) !!}
                            </div>
  
                            <div class="col-md-6">
                                {!! Form::label('subtitle', 'Subtítulo') !!}
                                {!! Form::text('subtitle', null, ['class' => 'form-control', 'placeholder' => 'Subtítulo']) !!}
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-md-6">
                                {!! Form::label('message', 'Mensagem') !!}
                                {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Mensagem']) !!}
                            </div>

                            <div class="col-md-6">
                                <div>{!! Form::label('tags', 'Lista de Tags para serem utilizadas nos pushs') !!}</div>
                                <ul class="list-tags">
                                    <li><strong data-tag="[DATA]">[DATA]</strong> - Data do vencimento do pacote</li>
                                    <li><strong data-tag="[DIA_SEMANA]">[DIA_SEMANA]</strong> - Dia da semana do vencimento do pacote</li>
                                    <li><strong data-tag="[DIA_MES]">[DIA_MES]</strong> - Dia do vencimento do pacote</li>
                                    <li><strong data-tag="[NOME]">[NOME]</strong> - Nome do usúario</li>
                                    <li><strong data-tag="[PACK_NAME]">[PACK_NAME]</strong> - Nome do pacote</li>
                                    <li><strong data-tag="[PACK_PRICE]">[PACK_PRICE]</strong> - Valor do pacote</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row py-2">
                            <div class="col-6">
                                <h5>Tipo</h5>
                                <div class="row">
                                    <div class="col">
                                        {!! Form::radio('before', 1, true, ['id' => 'type-1', 'class' => 'radio-files']) !!}
                                        {!! Form::label('type-1', 'Antes') !!}
                                    </div>
                                    <div class="col">
                                        {!! Form::radio('before', 0, null, ['id' => 'type-0', 'class' => 'radio-files']) !!}
                                        {!! Form::label('type-0', 'Depois') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                {!! Form::label('days', 'Quantidade de dias') !!} 
                                {!! Form::select('days', $notification->daysRange, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>

@endsection