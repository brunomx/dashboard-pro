@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Criar Áudio
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Back
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::open(['route' => 'audio.store', 'method' => 'post', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            
                            <div class="col-md-6">
                                <label for="name">Link</label>
                                {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'link']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                <div>
                                    <label for="name">Audio (Formato MP3)</label>
                                </div>
                                {!! Form::file('path', ['class' => 'form-control', 'id' => 'audio-upload', 'style'=>'display:none']) !!}
                                <audio controls class="preview-audio"></audio>
                                {!! Form::button('Select', ['class' => 'btn btn-accent', 'id' => 'select-audio']) !!}  
                                {!! Form::button('Remove', ['class' => 'btn btn-danger', 'id' => 'remove-audio']) !!}  
                            </div>
                            <div class="col-md-6">
                                <label for="name">Imagem</label>
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                <img class="preview-image" />
                                {!! Form::button('Select', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}
                                {!! Form::button('Remove', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, null, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                            <div class="col-md-6">
                                <h5>Tipo</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::radio('type', 0, true, ['id' => 'type0']) !!}
                                        {!! Form::label('type0', 'Motivacional') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::radio('type', 1, false, ['id' => 'type1']) !!}
                                        {!! Form::label('type1', 'Produto') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-4 offset-md-8">
                                {!! Form::submit('Save', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.upload-error')
@endsection
@section('js-view')
    @include('templates.js.upload-image')
    @include('templates.js.upload-audio')
@endsection