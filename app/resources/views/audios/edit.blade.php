@extends('templates.master')
@section('content-view')

<div class="row py-4">
    <!-- Top Referrals Component -->
    <div class="col-lg-12 col-md-12 col-sm-12 mb-12">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">
                    Editar Áudio
                    <a href="{{ redirect()->back()->getTargetUrl() }}" class="text-right">
                        <button type="button" class="btn btn-primary btn-sm btn-add">
                            <i class="material-icons mr-1">arrow_back_ios</i>Voltar
                        </button>
                    </a>
                </h6>
            </div>
            <div class="card-body flex-column">
                {!! Form::model($audio, ['route' => ['audio.update', $audio->id], 'method' => 'PUT', 'files' => true]) !!}
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::label('name', 'Nome') !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                            </div>
                            
                            <div class="col-md-6">
                                {!! Form::label('link', 'Link') !!}
                                {!! Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'link']) !!}
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-6">
                                <div>
                                    {!! Form::label('path', 'Áudio (Formato MP3)') !!}
                                </div>
                                {!! Form::file('path', ['class' => 'form-control', 'id' => 'audio-upload', 'style'=>'display:none']) !!}
                                @if($audio->path)
                                    <audio controls class="preview-audio" style="display:block">
                                        <source src="{{ Storage::url($audio->path) }}" type="audio/mpeg">
                                    </audio>
                                @else
                                    <audio controls class="preview-audio"></audio>
                                @endif
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-audio']) !!}  
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-audio']) !!}
                                <input type="hidden" name="remove-audio" class="input-hidden" id="remove-hidden-audio" value="0">
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('image', 'Imagem') !!}
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image-upload', 'style'=>'display:none']) !!}
                                @if($audio->image)
                                    <img src={{ Storage::url($audio->image) }} class="preview-image"/>
                                @else
                                    <img class="preview-image"/>
                                @endif
                                {!! Form::button('Selecionar', ['class' => 'btn btn-accent', 'id' => 'select-image']) !!}
                                {!! Form::button('Remover', ['class' => 'btn btn-danger', 'id' => 'remove-image']) !!}
                                <input type="hidden" name="remove-image" class="input-hidden" id="remove-hidden-image" value="0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <label for="package_id">Restringir conteúdo ao pacote</label>
                                {!! Form::select('package_id', $packages, $audio->package == null ? null : $audio->package->id, ['class' => 'form-control', 'placeholder' => 'Todos os pacotes']) !!}
                            </div>
                            <div class="col-md-6">
                                <h5>Tipo</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! Form::radio('type', 0, true, ['id' => 'type0']) !!}
                                        {!! Form::label('type0', 'Motivacional') !!}
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::radio('type', 1, false, ['id' => 'type1']) !!}
                                        {!! Form::label('type1', 'Produto') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row py-3">
                            <div class="col-md-3 offset-md-9">
                                {!! Form::submit('Salvar', ['class' => 'btn btn-accent btn-block']) !!}                  
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- End Top Referrals Component -->
</div>
@include('templates.modal.upload-error')
@endsection
@section('js-view')
    @include('templates.js.upload-image')
    @include('templates.js.upload-audio')
@endsection