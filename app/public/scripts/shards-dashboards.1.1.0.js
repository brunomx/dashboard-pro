/*!
* Shards Dashboards v1.1.0
* Copyright 2011-2018 DesignRevision
* SEE LICENSE FILE
*/
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

window.ShardsDashboards = window.ShardsDashboards ? window.ShardsDashboards : {};

$.extend($.easing, {
  easeOutSine: function easeOutSine(x, t, b, c, d) {
    return c * Math.sin(t / d * (Math.PI / 2)) + b;
  }
});

$(document).ready(function () {
  var slideConfig = {
    duration: 270,
    easing: 'easeOutSine'
  };

  // Add dropdown animations when toggled.
  $(':not(.main-sidebar--icons-only) .dropdown').on('show.bs.dropdown', function () {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(slideConfig);
  });

  $(':not(.main-sidebar--icons-only) .dropdown').on('hide.bs.dropdown', function () {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(slideConfig);
  });

  /**
   * Sidebar toggles
   */
  $('.toggle-sidebar').click(function (e) {
    $('.main-sidebar').toggleClass('open');
  });

  $('.list-tags strong').on('click', function(e){
    let tag = $(e.currentTarget).data('tag'),
      text = $('#message').val(),
      lastChar = text[text.length - 1];
    if (lastChar == ' ' || lastChar == undefined){
      $('#message').val(text + tag);
    } else {
      $('#message').val(text + ' ' + tag);
    }
  });

  $('#deleteModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget),
        modal = $(this);
    modal.find('.modal-body strong').text(button.data('name'));
    modal.find('.modal-footer .input-hidden[name=id]').val(button.data('id'));
    modal.find('.modal-footer .input-hidden[name=entity_id]').val(button.data('entity'));
  });

  $('#listModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget),
      modal = $(this);
    $('#listModalLabel').text(button.data('name'));
    getList(button.data('id'), $('#listModal').data('route'), button);
  });

  function getList(id, url, button) {
    var tbody = $('#listModal tbody');
    tbody.empty();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: '/' + url,
      type: 'POST',
      data: {id: id},
      dataType: 'JSON',
      success: function (result) {
        if (result.success){
          $('#listModalLabel').html(button.data('name') + ' - ' + result.data.length + ' visualizações');
          var i = 0;
          result.data.forEach(element => {
            i += 1;
            var tr = $('<tr>');
            $('<td>').text(i).appendTo(tr);
            $('<td>').text(element.name).appendTo(tr);
            $('<td>').append('<div>').find('div').addClass('progress progress-sm').append('<span>').find('span').width(element.completed == '1' ? '100%' : element.progress + '%').addClass('progress-bar').end().end().appendTo(tr);
            tbody.append(tr);
          });
        }
      },
      error: function (data) {
        console.log(data);
      }
    })
  }
});

})));